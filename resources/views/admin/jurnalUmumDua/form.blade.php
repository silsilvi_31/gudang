@extends('master.ella')
    
@section('content')
<div class="right_col" role="main">
  <div class="">
    <div class="clearfix"></div>
    <!-- Awal Modal Akun -->
    <div class="modal fade bs-example-modal-lg" id="modal_akun_debit" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
              <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Data Akun</h4>
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                </button>
              </div>
              <div class="modal-body">
                <table class="table table-striped table-bordered" id="tb_akun_debit" style="width:100%">
                      <thead>
                          <tr>
                              <th>No.</th>
                              <th>No. Akun</th>
                              <th>Nama</th>
                              <th>Opsi</th>
                          </tr>
                      </thead>
                      <tbody>
                      </tbody>
                  </table>
              </div>
          </div>
      </div>
    </div>
    <!-- Akhir Modal Akun -->

    <!-- Awal Modal Akun -->
    <div class="modal fade bs-example-modal-lg" id="modal_akun_kredit" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
              <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Data Akun</h4>
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                </button>
              </div>
              <div class="modal-body">
                <table class="table table-striped table-bordered" id="tb_akun_kredit" style="width:100%">
                      <thead>
                          <tr>
                              <th>No.</th>
                              <th>No. Akun</th>
                              <th>Nama</th>
                              <th>Opsi</th>
                          </tr>
                      </thead>
                      <tbody>
                      </tbody>
                  </table>
              </div>
          </div>
      </div>
    </div>
    <!-- Akhir Modal Akun -->

    <!-- ============================================================== -->
      <div class="row">
        <div class="col-md-12 col-sm-12">
          <div class="x_panel">
            <div class="x_title">
              <h2>Jurnal Umum Lain</h2>
              <ul class="nav navbar-right panel_toolbox">
                <a href=" {{ route('jurnalUmum.form') }} " class="btn btn-sm btn-success">New</a>
                <a href=" {{  route('jurnalUmum.index') }} " class="btn btn-sm btn-secondary"><i class="fa fa-arrow-left"></i> Back </a>
              </ul>
              <div class="clearfix"></div>
            </div>

            <div class="x_content">
              <br/>
              <form id="" action="#" method="#" data-parsley-validate class="form-verical form-label-left">
                {{-- {{ csrf_field() }} --}}
                {{-- Kiri --}}
                <div class="col-md-6 col-sm-6">
                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">No. Jurnal <span class="required">*</span>
                    </label>
                    <div class="col-md-2 col-sm-2 ">
                      <input type="text" id="Form" name="form" required="required" class="form-control" readonly value="{{ !empty($form) ? $form : ''  }}">
                      <input type="text" id="noJurnalUmum" name="no_jurnalUmum" required="required" class="form-control" readonly  value="{{ !empty($id_ju) ? $id_ju : '' }}">
                    </div>
                  </div>
                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Tanggal
                    </label>
                    <div class="col-md-6 col-sm-6 ">
                      <input type="text" id="Tgl" name="tgl" required='required' class="form-control" value="{{ !empty($tgl) ? $tgl : '' }}">
                    </div>
                  </div>
                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Nama <span class="required">*</span>
                    </label>
                    <div class="col-md-6">
                      <input type="text" id="Nama" name="nama" required='required' class="form-control" value="{{ !empty($nama) ? $nama : '' }}">
                    </div>
                  </div>
                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">No. Akun Debit
                    </label>
                    <div class="col-md-6 ">
                      <input type="text" id="noAkunDebit" name="no_akun_debit" class="form-control noAkunD" value="{{ !empty($no_akun) ? $no_akun : '' }}">
                    </div>
                  </div>
                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Nama Akun
                    </label>
                    <div class="col-md-6 ">
                      <input type="text" id="namaAkunDebit" name="nama_akun_debit" class="form-control" readonly value={{ !empty($nama_akun) ? $nama_akun : '' }}>
                    </div>
                  </div>
                </div>

                <div class="clearfix"></div>
                <div class="ln_solid"></div>
                {{-- kiri --}}
                <div class="col-md-6 col-sm-6">
                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Nama Barang
                    </label>
                    <div class="col-md-6 ">
                      <input type="text" id="idItem" name="id_item" class="form-control" autocomplete="off" readonly>
                      <input type="text" id="idBrg" name="id_brg" class="form-control" autocomplete="off" readonly>
                      <input type="text" id="namaBrg" name="nama_brg" class="form-control" autocomplete="off">
                    </div>
                    {{-- <div class="col-md-1 ">
                      <button type="button" class="btn btn-sm btn-info barangQ"><i class="fa fa-plus"></i></button>
                    </div> --}}
                  </div>
                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">No. Akun
                    </label>
                    <div class="col-md-6 ">
                      <input type="text" id="noAkunKredit" name="no_akun_kredit" class="form-control noAkunK">
                    </div>
                  </div>
                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Nama Akun
                    </label>
                    <div class="col-md-6 ">
                      <input type="text" id="namaAkunKredit" name="nama_akun_kredit" class="form-control" readonly>
                    </div>
                  </div>
                </div>  

                {{-- kanan --}}
                <div class="col-md-6 col-sm-6">
                  {{-- <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">m3
                    </label>
                    <div class="col-md-6 col-sm-6 ">
                      <input type="text" id="M3" name="m3" class="form-control">
                    </div>
                  </div> --}}
                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Qty
                    </label>
                    <div class="col-md-6 col-sm-6 ">
                      <input type="text" id="Qty" name="qty" class="form-control qtyQ">
                    </div>
                  </div>
                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Harga
                    </label>
                    <div class="col-md-6 col-sm-6 ">
                      <input type="text" id="Harga" name="harga" class="form-control hargaQ">
                    </div>
                  </div>
                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Subtotal
                    </label>
                    <div class="col-md-6 col-sm-6 ">
                      <input type="text" id="Subtotal" name="subtotal" class="form-control" autocomplete="off" readonly>
                    </div>
                  </div>
                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Ketr Akun
                    </label>
                    <div class="col-md-6 col-sm-6 ">
                      <input type="text" id="ketrAkun" name="ketr_akun" class="form-control">
                    </div>
                  </div>
                </div>

                <div class="clearfix"></div>
                <div class="ln_solid"></div>
                <div class="col-md-6 col-sm-6">
                  <div class="form-group">
                    <div class="offset-md-3">
                      {{-- <button type="button" class="btn btn-primary">Cancel</button> --}}
                      <button class="btn btn-sm btn-primary" type="reset">Reset</button>
                      <button type="button" class="btn btn-sm btn-success" onclick="save_jurnal()">Submit</button>
                    </div>
                  </div>
                </div>
              </div>                                   
              </form>

              <div class="clearfix"></div>
                <div class="ln_solid"></div>
                <table class="table table-striped table-bordered" id="tbJurnalUmum" style="width:100%">
                  <thead>
                      <tr>
                        <th>No. </th>
                        <th>Nama Barang</th>
                        <th>Satuan</th>
                        <th>Harga</th>
                        <th>Qty</th>
                        <th>Keterangan</th>
                        <th>Opsi</th>
                      </tr>
                  </thead>
                  <tbody>
                  </tbody>
              </table>

              <div class="clearfix"></div>
              <div class="ln_solid"></div>
              </div>
          </div>
        </div>
      </div>
  </div>
</div>        
@endsection

@push('js')
  <script type="text/javascript">
    $('#Tgl').datetimepicker({
        format: 'DD-MM-YYYY'
    });

    var form = $('#Form').val();
    if (form == 'edit') {

    } else {
      no_urut();
    }

    function no_urut() {
      $.ajax({
              type : 'GET',
              url : '{{ route('no_urut_jurnalUmum') }}',
              success : function (e) {
                console.log(e);
                $('#noJurnalUmum').val(e);
              }
      });
    }

    function hitung_subtotal(harga, qty) {
        var subtotal = ( harga * qty );
        $('#Subtotal').val(subtotal);
      }

      $(document).on('keyup', '.qtyQ', function(){
        var qty = $('#Qty').val();
        var harga = $('#Harga').val();
        hitung_subtotal(harga, qty);
      });

      $(document).on('keyup', '.hargaQ', function(){
        var qty = $('#Qty').val();
        var harga = $('#Harga').val();
        hitung_subtotal(harga, qty);
      });

    var tbJurnalUmum = '';

    tbJurnalUmum = $('#tbJurnalUmum').DataTable({
      processing    : true,
      serverSide    : true,
      ajax  : {
            type: 'POST',
            url : '{{ route('jurnalUmum.datatable_detail') }}', 
            data : function (e) {
              e._noJu = $('#noJurnalUmum').val();
            }
      },
      columns:[
        { data: 'DT_RowIndex', name: 'DT_RowIndex' },
        { data: 'nama_brg',        name: 'nama_brg' },
        { data: 'nama',        name: 'nama'},
        { data: 'harga',        name: 'harga'},
        { data: 'qty',    name: 'qty'},
        { data: 'ketr', name: 'ketr' },
        { data: 'opsi',       name: 'opsi' }
      ]
    });

    function save_jurnal() {
      var no_jurnal_umum = $('#noJurnalUmum').val();
      var tgl = $('#Tgl').val();
      var nama = $('#Nama').val();
      var no_akun_debit = $('#noAkunDebit').val();
      
      var nama_brg = $('#namaBrg').val();
      var no_akun_kredit = $('#noAkunKredit').val();
      var qty = $('#Qty').val();
      var harga = $('#Harga').val();
      var subtotal = $('#Subtotal').val();
      var ket = $('#ketrAkun').val();

      $.ajax({
        type  : 'POST',
        url   : '{{ route('jurnalUmumDua.save') }}',
        data  : {
                  '_noJurnalUmum' : no_jurnal_umum,
                  '_tgl' : tgl,
                  '_nama' : nama,
                  '_noAkunDebit' : no_akun_debit,
                  '_namaBrg' : nama_brg,
                  '_noAkunKredit' : no_akun_kredit,
                  '_qty' : qty,
                  '_harga' : harga,
                  '_subtotal' : subtotal,
                  '_ketr' : ket
                },
        success  : function (e) {
          notif(e.code,e.msg);
          clear_akun();
          tbJurnalUmum.draw()
        }
      });
    }

    function clear_akun() {
      $('#noAkunKredit').val('');
      $('#namaAkunKredit').val('');
      $('#namaBrg').val('');
      $('#Qty').val('');
      $('#Harga').val('');
      $('#Subtotal').val('');
      $('#ketrAkun').val('');
    }

    $(document).on('click', '.noAkunD', function(){  
      $('#modal_akun_debit').modal('show');  
    });

    $(document).on('click', '.noAkunK', function(){  
      $('#modal_akun_kredit').modal('show');  
    });

    var tb_akun_debit = '';

    tb_akun_debit = $('#tb_akun_debit').DataTable({
      processing    : true,
      serverSide    : true,
      ajax  : {
            type: 'GET',
            url : '{{ route('jurnalUmum.datatable_akun_debit') }}', 
      },
      columns:[
        { data: 'DT_RowIndex', name: 'DT_RowIndex' },
        { data: 'no_akun', data: 'no_akun' },
        { data: 'akun', data: 'akun'},
        { data: 'opsi', data: 'opsi'}
      ]
    });

    var tb_akun_kredit = '';

    tb_akun_kredit = $('#tb_akun_kredit').DataTable({
      processing    : true,
      serverSide    : true,
      ajax  : {
            type: 'GET',
            url : '{{ route('jurnalUmum.datatable_akun_kredit') }}', 
      },
      columns:[
        { data: 'DT_RowIndex', name: 'DT_RowIndex' },
        { data: 'no_akun', data: 'no_akun' },
        { data: 'akun', data: 'akun'},
        { data: 'opsi', data: 'opsi'}
      ]
    });

    function select_akun(no_akun, akun) {
      $('#modal_akun_debit').modal('hide');
      $('#noAkunDebit').val(no_akun);
      $('#namaAkunDebit').val(akun);
    }

    function select_akun_kredit(no_akun, akun) {
      $('#modal_akun_kredit').modal('hide');
      $('#noAkunKredit').val(no_akun);
      $('#namaAkunKredit').val(akun);
    }

    function select_brg(kode_brg,nama_brg,harga, id_satuan, satuan, no_akun) {
      $('#modal_brg').modal('hide');
      $('#idBrg').val(kode_brg);
      $('#namaBrg').val(nama_brg);
      $('#Harga').val(harga);
      $('#idSatuan').val(id_satuan);
      $('#Satuan').val(satuan);
      $('#noAkunDebit').val(no_akun)
      clear_barang2();
    }   

    function clear_barang2() {
      $('#Qty').val('');
      $('#Ketr').val('');
      $('#Subtotal').val('');
      $('#idSatuan').val('');
    }

    function delete_item(id, id_item) {
      $.ajax({
        type        : 'POST',
        url         : '{{ route('jurnalUmum.delete_item') }}',
        data        : { 
                        '_idJu' : id,
                        '_idItem' : id_item
                      },
        success     : function (e) {
          notif(e.response.code, e.response.msg);
          tbJurnalUmum.draw();
        }
      });
    }

  </script>
@endpush