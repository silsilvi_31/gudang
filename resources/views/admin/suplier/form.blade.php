@extends('master.monster')
@section('title', 'Resep')
{{-- @section('menu1', 'active') --}}

@section('content')
<div class="container-fluid">
    <div class="row page-titles">
        <div class="col-md-6 col-8 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0">Dashboard</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item">Suplier</li>
                <li class="breadcrumb-item active">Form</li>
            </ol>
        </div>
    </div>
    
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-block">
                    <form class="form-horizontal form-material" method="POST" action="{{ route('suplier.save') }}">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label class="col-md-12">Nama Suplier</label>
                            <div class="col-md-12">
                                <input type="text" class="form-control form-control-line" name="nama" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="example-email" class="col-md-12">Alamat</label>
                            <div class="col-md-12">
                                <input type="text" class="form-control form-control-line" name="alamat" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-12">No. Telp</label>
                            <div class="col-md-12">
                                <input type="text" class="form-control form-control-line" name="no_telp" required>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <div class="col-sm-12">
                                <button class="btn btn-success pull-right"> Save</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('js')
    <script type="text/javascript">  
        console.log('supplier');
        
    </script>
@endpush