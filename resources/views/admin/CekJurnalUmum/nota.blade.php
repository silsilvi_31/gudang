@extends('master.print')
    
@section('content')
<style type="text/css">
  body {
    /* border: 1px solid #000 !important; */
    font-size : 90px !important;
    font-family: "Arial Narrow", sans-serif !important;
    font-weight: 500 !important;
    color : black !important;

  }
  .table-bordered, .table th {
    /* border: 1px solid #000 !important; */
    border : none !important;
    padding: 0px;
  }

  .table td {
    border: none !important;
    padding: 2px;
    margin-bottom: 
  }
  .table {
    margin-bottom: 0px !important;
  }
  .nominal {
    float: right;
  }
  .x-panel {
    max-width: 100%;
    /* width: 30%; */
    /* background-color: pink; */
  }

</style>
<div class="right_col" role="main">
  <div class="">
    <div class="clearfix"></div>
    {{-- notif --}}
    @if (session('response'))
        @push('js')
          <script type="text/javascript">
            notif({{ session('response')['code'] }}, "{{ session('response')['msg'] }}");
          </script>
        @endpush
    @endif
    {{-- notif --}}

        <!-- page content -->

            <div class="x-panel">
              <center>TOKO INA</center>
                    <table class="table">
                      <tr>
                        <td>Tanggal</td>
                        <td colspan="3">: {{ $tgl }}</td>
                      </tr>
                      {{-- <tr>
                        <td>No. Nota</td>
                        <td colspan="3">: {{ $id }}</td>
                      </tr> --}}
                      <tr>
                        <td>Kasir</td>
                        <td colspan="3">: {{ $mengetahui }}</td>
                      </tr>
                    </table>
                    <div class="clearfix">
                      =====================================
                    </div>
                    <table class="table">
                      <tbody>
                        @foreach ($detail_barang as $item)
                        <tr>
                          <td colspan="4"> {{ $item->nama_brg }} </td>
                        </tr>
                        <tr>
                          <td></td>
                          <td colspan="2"> <center>{{ $item->qty }} </center> </td>
                          <td >{{ number_format($item->harga, 0, ',', '.') }}</td>
                          <td style="float:right;"> {{ number_format((($item->qty * $item->harga)), 0,',','.') }} </td>
                        </tr>
                        <tr>
                        @if ((isset($item->potongan)) && ($item->potongan !=0))
                          <td colspan="4">Diskon - {{ number_format($item->potongan, 0, ',', '.') }}</td>
                          {{-- <td style="float:right">{{ number_format((($item->qty * $item->potongan)), 0,',','.') }} </td> --}}
                        @endif
                        </tr>
                      @endforeach
                      
                      </tbody>
                      <table class="table">
                      <div class="clearfix">
                        =====================================
                      </div>
                      <tr>
                        <td colspan="4"><b>Total Item</b></td>
                        <td id="totalQty" > <center>{{ number_format($total_qty, 0,',','.') }}</center>  </td>
                        <td></td>
                        <td id="grandtotal" style="float:right;"> {{ number_format($totalq, 0,',','.') }}</td>
                      </tr>

                      @if ((isset($total_pot)) && ($total_pot !=0))
                        <tr>
                          <td colspan="4"><b>Total Diskon</b></td>
                          <td></td>
                          <td></td>
                          <td style="float:right;"> {{ number_format($total_pot, 0,',','.') }}</td>
                        </tr>
                      @endif
                      
                      <tr>
                        <td colspan="4"><b>Total Belanja</b></td>
                        <td></td>
                        <td></td>
                        <td id="grandtotal" style="float:right;"> {{ number_format($total, 0,',','.') }}</td>
                      </tr>
                      <tr>
                        <td colspan="4"><b>Tunai</b></td>
                        <td></td>
                        <td></td>
                        <td id="tunai" style="float:right;"> {{ number_format($bayar, 0,',','.') }}</td>
                      </tr>
                      <tr>
                        <td colspan="4"><b>Kembali</b></td>
                        <td></td>
                        <td></td>
                        <td id="kembali" style="float:right;"> {{ number_format($kembalian, 0,',','.') }}</td>
                      </tr>
                    </table>
                    <br>
                    <center>** LUNAS ** </center>
                    <br>
                    <br>
                    <br>
                    
            </div>
        <!-- /page content -->
</div>

@endsection

@push('js')
  <script type="text/javascript">
    $(function(){
      window.print();
    }); 
  </script>
@endpush