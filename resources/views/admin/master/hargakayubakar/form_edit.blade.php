@extends('master.ella')
    
@section('content')
<div class="right_col" role="main">
  <div class="">
    <div class="clearfix"></div>
      <div class="row">
        <div class="col-md-12 col-sm-12">
          <div class="x_panel">
            <div class="x_title">
              <h2>Edit Harga Kayu Bakar</h2>
              <div class="clearfix"></div>
            </div>
            <div class="x_content">
              <br />
              <form id="" action="{{ route('hargakayubakar.update') }}" method="POST" data-parsley-validate class="form-horizontal form-label-left">
                {{ csrf_field() }}
                <div class="item form-group">
                  <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name"> Pelanggan <span class="required">*</span>
                  </label>
                  <div class="col-md-6 col-sm-6 ">
                  <input type="hidden" name="kode_barang" value="{{ $select_barang->kode }}">
                    <select name="pelanggan" id="pelanggan" class="form-control ">
                      <option name='pelanggan_kosong' id="pelanggan_kosong" value="">-</option>
                     
                      @foreach ($kepada as $item)
                        <option value="{{ $item->id }}" @if( $item->id == $select_barang->id_bkkepada ) selected @endif>{{ $item->kepada }}</option>
                      @endforeach
                      
                    </select>
                  </div>
                  {{-- <div class="col-md-1">
                    <button type="button" class="btn btn-info kepadaQ"><i class="fa fa-plus"></i></button>
                  </div> --}}
                </div>
                {{-- <div class="item form-group">
                  <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name"> No. Akun Pelanggan <span class="required">*</span>
                  </label>
                  <div class="col-md-6 col-sm-6 ">
                    <input type="text" id="" name="akunpelanggan" required="required" class="form-control " autocomplete="off">
                  </div>
                </div> --}}
                {{-- <div class="item form-group">
                  <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name"> Barang <span class="required">*</span>
                  </label>
                  <div class="col-md-6 col-sm-6 ">
                    <select name="barang" id="" class="form-control ">
                      <option value="">-</option>
                      @foreach ($barang as $item)
                        <option value="{{ $item->kode }}">{{ $item->nama_brg }}</option>
                      @endforeach
                    </select>
                  </div>
                </div> --}}
                <div class="item form-group">
                  <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name"> Barang <span class="required">*</span>
                  </label>
                  <div class="col-md-6 col-sm-6 ">
                    <input type="text" id="barang" name="barang" required="required" class="form-control " autocomplete="off" value="{{ $select_barang->nama_brg }}">
                    ex : kayu bakar sabetan triplek @truck
                  </div>
                </div>
                <div class="item form-group">
                  <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name"> Satuan <span class="required">*</span>
                  </label>
                  <div class="col-md-6 col-sm-6 ">
                    <select name="id_satuan" id="id_satuan" class="form-control " autocomplete="off">
                      <option value="">-</option>
                      @foreach ($satuan as $item)
                        <option value="{{ $item->id }}" @if($item->id == $select_barang->satuan) selected @endif>{{ $item->nama }}</option>
                      @endforeach
                    </select>
                  </div>
                </div>
                <div class="item form-group">
                  <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name"> Harga <span class="required">*</span>
                  </label>
                  <div class="col-md-6 col-sm-6 ">
                    <input type="text" id="harga" name="harga" required="required" class="form-control " autocomplete="off" value="{{ $select_barang->harga }}">
                  </div>
                </div>
                <div class="ln_solid"></div>
                <div class="item form-group">
                  <div class="col-md-6 col-sm-6 offset-md-3">
                    <a href="{{ route('hargakayubakar.index') }}" class="btn btn-warning" type="button">Cancel</a>
                    <button type="submit" class="btn btn-success">Submit</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
  </div>
</div>        
@endsection

@push('js')
  <script type="text/javascript">
    console.log('form');
    // var datatablePresensi = $('#tbJabatan').DataTable({
    //     processing: true,
    //     serverSide: true,
    //     ajax: {
    //         type: 'GET',
    //         url: '{{ route('jabatan.datatable') }}', 
    //     },
    //     columns: [
    //       { data: 'DT_RowIndex', name: 'DT_RowIndex' },
    //       { data: 'jabatan', name: 'jabatan' },
    //       { data: 'opsi', name: 'opsi' }
    //     ]
    // });

  </script>
@endpush