@extends('master.ella')
    
@section('content')
<div class="right_col" role="main">
  <div class="">
    <div class="clearfix"></div>
    
    {{-- notif --}}
    @if (session('response'))
        @push('js')
          <script type="text/javascript">
            notif({{ session('response')['code'] }}, "{{ session('response')['msg'] }}");
          </script>
        @endpush
    @endif
    {{-- notif --}}

    <div class="row">
      <div class="col-md-12 col-sm-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Kendaraan</h2>
            <ul class="nav navbar-right panel_toolbox">
              
              <a href=" {{ route('kendaraan.form') }} " class="btn btn-sm btn-success"><i class="fa fa-plus"></i></a>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <div class="row">
              <div class="col-md-12">
                <div class="card-box table-responsive">
                  <table id="tbKendaraan" class="table table-striped table-bordered" style="width:100%">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>Kendaraan</th>
                        <th>No Plat</th>
                        <th>KIR</th>
                        <th>Tahunan</th>
                        <th>5 Tahunan</th>
                        <th>Penyusutan</th>
                        <th>Opsi</th>
                      </tr>
                    </thead>
                    <tbody>
                      
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    
  </div>
</div>
        
@endsection

@push('js')
  <script type="text/javascript">
    // console.log('uyeee');
    var datatableKendaraan = $('#tbKendaraan').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            type: 'GET',
            url: '{{ route('kendaraan.datatable') }}', 
        },
        columns: [
          { data: 'DT_RowIndex', name: 'DT_RowIndex' },
          { data: 'nama', name: 'kendaraan' },
          { data: 'no_plat', name: 'no_plat' },
          { data: 'kir', name: 'kir' },
          { data: 'tahunan', name: 'tahunan' },
          { data: 'lima_tahunan', name: 'lima_tahunan' },
          { data: 'penyusutan', name: 'penyusutan' },
          { data: 'opsi', name: 'opsi' }
        ]
    });

    function delete_kendaraan(id) {
      $.ajax({
        type    : 'POST',
        url     : '{{ route('kendaraan.delete') }}',
        data    : { '_idKendaraan' : id },
        success : function (e) {
          notif(e.response.code, e.response.msg);
          datatableKendaraan.draw(); 
        }
      });
    }

  </script>
@endpush