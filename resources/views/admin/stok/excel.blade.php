{{-- @extends('master.print')
    
@section('content') --}}
<table>
        <tr>
            {{-- <th>Tgl</th> --}}
            <th>No. </th>
            <th>Harga</th>
            <th>Nama Barang</th>
            <th>Awal</th>
            <th>Masuk</th>
            <th>Keluar</th>
            <th>Retur Jual</th>
            <th>Retur Beli</th>
            <th>Total</th>
            <th>Sisa</th>
        </tr>
        @foreach ($data as $value)
            <tr>
                <td>{{ $value->no }}</td>
                <td>{{ $value->harga }}</td>
                <td>{{ $value->nama_brg }}</td>
                <td>{{ $value->stok_awal }}</td>
                <td>{{ $value->stok_masuk }}</td>
                <td>{{ $value->stok_keluar }}</td>
                <td>{{ $value->stok_jual_retur }}</td>
                <td>{{ $value->stok_beli_retur }}</td>
                <td>{{ $value->total }}</td>
                <td>{{ $value->sisa }}</td>
            </tr>
        @endforeach
    </table>
{{-- @endsection --}}