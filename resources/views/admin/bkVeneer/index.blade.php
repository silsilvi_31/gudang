@extends('master.ella')
    
@section('content')
<div class="right_col" role="main">
  <div class="">
    <div class="clearfix"></div>
    
    {{-- notif --}}
    @if (session('response'))
        @push('js')
          <script type="text/javascript">
            notif({{ session('response')['code'] }}, "{{ session('response')['msg'] }}");
          </script>
        @endpush
    @endif
    {{-- notif --}}

      <div class="col-md-12 col-sm-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Bk Veneer Gudang</h2>
            <ul class="nav navbar-right panel_toolbox">
              <a href="{{ route('bkVeneer.form') }}" class="btn btn-sm btn-success"><i class="fa fa-plus"></i></a>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <div class="row">
              <div class="col-md-12">
                  <table id="tb_bkVeneer" class="table table-sm table-striped table-bordered" style="width:100%">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>No BK</th>
                        <th>Tanggal</th>
                        <th>kepada</th>
                        <th>Total</th>
                        <th>Cek BK</th>
                        <th>Opsi</th>
                      </tr>
                    </thead>
                    <tbody>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    
  </div>
</div>
        
@endsection

@push('js')
  <script type="text/javascript"> 
    var tb_bkVeneer = '';

    tb_bkVeneer = $('#tb_bkVeneer').DataTable({
      processing    : true,
      serverSide    : true,
      ordering      : false,
      ajax  : {
          type : 'GET',
          url : '{{ route('bkVeneer.datatable') }}',

      },
      columns: [
        { data: 'DT_RowIndex', name: 'DT_RowIndex' },
        { data: 'no_bk', name: 'no_bk' },
        { data: 'tgl', name: 'tgl' },
        { data: 'kepada', name: 'kepada' },
        { data: 'total', name: 'total' },
        { data: 'is_cek_bk', name: 'is_cek_bk' },
        { data: 'opsi', name: 'opsi' }
      ]
    });

    function delete_bk(no_bk) {
      $.ajax({
        type    : 'POST',
        url     : '{{ route('bkVeneer.delete') }}',
        data    : { '_noBk' : no_bk },
        success : function (e) {
          notif(e.response.code, e.response.msg);
          tb_bkVeneer.draw(); 
        }
      });
    }
  </script>
@endpush