@extends('master.ella')
    
@section('content')
<div class="right_col" role="main">
  <div class="">
    <div class="clearfix"></div>
       <!-- Awal Modal Barang -->
    <div class="modal fade bs-example-modal-lg" id="modal_brg" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
              <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Data Barang</h4>
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                </button>
              </div>
              <div class="modal-body">
                <table class="table table-striped table-bordered" id="tabel_brg" style="width:100%">
                      <thead>
                          <tr>
                              <th>No.</th>
                              <th>Nama</th>
                              <th>Veneer</th>
                              <th>Jenis Veneer</th>
                              {{-- <th>Harga</th> --}}
                              {{-- <th>Satuan</th> --}}
                              <th>Opsi</th>
                          </tr>
                      </thead>
                      <tbody>
                      </tbody>
                  </table>
              </div>
          </div>
      </div>
    </div>
    <!-- Awal Modal barang -->

    <!-- ============================================================== -->
      <div class="row">
        <div class="col-md-12 col-sm-12">
          <div class="x_panel">
            <div class="x_title">
              <h2>Form BK Veneer</h2>
              <ul class="nav navbar-right panel_toolbox">
                <a href=" {{ route('bkVeneer.form') }} " class="btn btn-sm btn-success">New</a>
                <a href=" {{  route('bkVeneer.index') }} " class="btn btn-sm btn-secondary"><i class="fa fa-arrow-left"></i> Back </a>
              </ul>
              <div class="clearfix"></div>
            </div>

            <div class="x_content">
              <br/>
              <form id="" action="#" method="#" data-parsley-validate class="form-verical form-label-left">
                {{-- {{ csrf_field() }} --}}
                {{-- Kiri --}}
                <div class="col-md-6 col-sm-6">
                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">No BK <span class="required">*</span>
                    </label>
                    <div class="col-md-6 col-sm-6 ">
                      <input type="text" id="Form" name="form" required="required" class="form-control" value="{{ !empty($form) ? $form : ''}}" readonly>
                      <input type="text" id="noBk" name="no_bk" required="required" class="form-control" readonly value="{{ !empty($no_bk) ? $no_bk : ''}}">
                    </div>
                  </div>
                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Tanggal
                    </label>
                    <div class="col-md-6 col-sm-6 ">
                      <input type="text" id="Tgl" name="tgl" required='required' class="form-control" value="{{ !empty($tgl) ? $tgl : ''}}" >
                    </div>
                  </div>
                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Kepada <span class="required">*</span>
                    </label>
                    <div class="col-md-6">
                      @php
                          $kepadaQ = !empty($id_kepada) ? $id_kepada : '';
                      @endphp
                      <select class="form-control" id="idKepada" name="id_kepada" autocomplete="off" required='required'>
                        <option value="" id="kepada_kosong" >-</option>
                        @foreach ($kepada as $item)
                          <option value="{{ $item->id }}" @if($item->id == $kepadaQ) selected @endif>{{ $item->kepada }}</option>                              
                          {{-- <option value="{{ $item->id }}" >{{ $item->kepada }}</option>                               --}}
                        @endforeach
                      </select>
                    </div>
                  </div>
                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Ketr <span class="required">*</span>
                    </label>
                    <div class="col-md-6 col-sm-6 ">
                      <input type="text" id="Ketr" name="ketr" required="required" class="form-control" autocomplete="off" value="{{ !empty($ketr) ? $ketr : '' }}">
                    </div>
                  </div>
                </div>

                <div class="clearfix"></div>
                <div class="ln_solid"></div>
                {{-- kiri --}}
                <div class="col-md-6 col-sm-6">
                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Nama Barang
                    </label>
                    <div class="col-md-6 col-sm-6">
                      <input type="text" id="idItem" name="id_item" class="form-control" autocomplete="off" readonly>
                      <input type="text" id="idBrg" name="id_brg" class="form-control" autocomplete="off" readonly>
                      <input type="text" id="namaBrg" name="nama_brg" class="form-control brgQ" autocomplete="off">
                    </div>
                  </div>
                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Jenis Veneer
                    </label>
                    <div class="col-md-6 col-sm-6 ">
                      <input type="text" id="jenisVeneer" name="jenis_veneer" class="form-control" autocomplete="off" readonly>
                    </div>
                  </div>
                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name"> p x l x t
                    </label>
                    <div class="col-md-2 col-sm-6 ">
                      <input type="text" id="Panjang" name="panjang" class="form-control" value="" readonly autocomplete="off">
                    </div>
                    <div class="col-md-2 col-sm-6 ">
                      <input type="text" id="Lebar" name="lebar" class="form-control" value="" readonly autocomplete="off">
                    </div>
                    <div class="col-md-2 col-sm-6 ">
                      <input type="text" id="Tebal" name="tebal" class="form-control" value="" readonly autocomplete="off">
                    </div>
                  </div>
                </div>  

                {{-- kanan --}}
                <div class="col-md-6 col-sm-6">
                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name"> Satuan
                    </label>
                    <div class="col-md-6 col-sm-6 ">
                      <select id="idSatuan"  name="id_satuan" class="form-control" autocomplete="off">
                        <option value="">-</option>
                        @foreach ($satuan as $item)
                          <option value="{{ $item->id }}">{{ $item->nama }}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Qty
                    </label>
                    <div class="col-md-6 col-sm-6 ">
                      <input type="text" id="Qty" name="qty" class="form-control qtyQ" autocomplete="off">
                    </div>
                  </div>
                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Harga
                    </label>
                    <div class="col-md-6 col-sm-6 ">
                      <input type="text" id="Harga" name="harga" class="form-control" autocomplete="off" readonly>
                    </div>
                  </div>
                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">M3
                    </label>
                    <div class="col-md-6 col-sm-6 ">
                      <input type="text" id="M3" name="m3" class="form-control" autocomplete="off" readonly>
                    </div>
                  </div>
                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Subtotal
                    </label>
                    <div class="col-md-6 col-sm-6 ">
                      <input type="text" id="Subtotal" name="subtotal" class="form-control" autocomplete="off" readonly>
                    </div>
                  </div>
                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Keterangan
                    </label>
                    <div class="col-md-6 col-sm-6 ">
                      <input type="text" id="Keterangan" name="keterangan" class="form-control" autocomplete="off">
                    </div>
                  </div>
                </div>

                <div class="clearfix"></div>
                <div class="ln_solid"></div>
                <div class="col-md-6 col-sm-6">
                  <div class="form-group">
                    <div class="offset-md-3">
                      {{-- <button type="button" class="btn btn-primary">Cancel</button> --}}
                      <button class="btn btn-sm btn-primary" type="reset">Reset</button>
                      <button type="button" class="btn btn-sm btn-success" onclick="save()">Submit</button>
                    </div>
                  </div>
                </div>
              </div>                                   
              </form>

              <div class="clearfix"></div>
                <div class="ln_solid"></div>
                <table class="table table-striped table-bordered" id="tb_detail" style="width:100%">
                  <thead>
                      <tr>
                          <th>No.</th>
                          <th>Nama Barang</th>
                          <th>Keterangan</th>
                          <th>Satuan</th>
                          <th>Harga</th>
                          <th>Qty</th>
                          <th>M3</th>
                          <th>Subtotal</th>
                          <th>Opsi</th>
                      </tr>
                  </thead>
                  <tbody>
                  </tbody>
                  <tfoot>
                    <td colspan="5" style="text-align:right"> <strong>Total:</strong> </td>
                    <td></td>
                    <td></td>
                    <td></td>
                  </tfoot>
              </table>

              <div class="clearfix"></div>
              <div class="ln_solid"></div>
              </div>
          </div>
        </div>
      </div>
  </div>
</div>        
@endsection

@push('js')
  <script type="text/javascript">
    $('#Tgl').datetimepicker({
        format: 'DD-MM-YYYY'
    });

    var form = $('#Form').val();
    if (form == 'edit') {

    } else {
      no_urut();
    }

    function no_urut() {
      $.ajax({
              type : 'GET',
              url : '{{ route('no_bk') }}',
              success : function (e) {
                console.log(e);
                $('#noBk').val(e);
              }
      });
    }

    function num_format(nStr)
    {
        nStr += '';
        x = nStr.split('.');
        x1 = x[0];
        x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + '.' + '$2');
        }
        return x1 + x2;
    }

    function num_format2(nStr)
    {
        nStr += '';
        x = nStr.split(',');
        x1 = x[0];
        x2 = x.length > 1 ? ',' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + ',' + '$2');
        }
        return x1 + x2;
    }

    function save() {
      var no_bk = $('#noBk').val();
      var id_kepada = $('#idKepada').val();
      var tgl = $('#Tgl').val();
      var ketr = $('#Ketr').val();
      
      var id_item = $('#idItem').val();
      var id_brg = $('#idBrg').val();
      var nama_brg = $('#namaBrg').val();
      var jenis_veneer = $('#jenisVeneer').val();
      var id_satuan = $('#idSatuan').val();
      var qty = $('#Qty').val();
      var harga = $('#Harga').val();
      var m3 = $('#M3').val();
      var subtotal = $('#Subtotal').val();
      var keterangan = $('#Keterangan').val();

      $.ajax({
        type  : 'POST',
        url   : '{{ route('bkVeneer.save') }}',
        data  : {
                  '_noBk' : no_bk,
                  '_idKepada' : id_kepada,
                  '_tgl' : tgl,
                  '_ketr' : ketr,
                  '_idItem' : id_item,
                  '_idBrg' : id_brg,
                  '_namaBrg' : nama_brg,
                  '_jenisVeneer' : jenis_veneer,
                  '_idSatuan' : id_satuan,
                  '_qty' : qty,
                  '_harga' : harga,
                  '_m3' : m3,
                  '_subtotal' : subtotal,
                  '_keterangan' : keterangan
                },
        success  : function (e) {
          tb_detail.draw();
          notif(e.code,e.msg);
          clear_akun();
        }
      });
    }

    function clear_akun() {
      $('#idBrg').val('');
      $('#namaBrg').val('');
      $('#jenisVeneer').val('');
      $('#Panjang').val('');
      $('#Lebar').val('')
      $('#Tebal').val('');
      $('#idSatuan').val('');
      $('#Qty').val('');
      $('#Harga').val('');
      $('#M3').val('');
      $('#Subtotal').val('');
      $('#Keterangan').val('');
    }

    $(document).on('click', '.brgQ', function(){  
      $('#modal_brg').modal('show');  
    });

    function select_brg(id,id_veneer,harga,panjang,lebar,tebal, jenis_veneer) {
      $('#modal_brg').modal('hide');
      $('#idBrg').val(id_veneer);
      $('#Harga').val(harga);
      $('#Panjang').val(panjang);
      $('#Lebar').val(lebar);
      $('#Tebal').val(tebal);
      $('#jenisVeneer').val(jenis_veneer)
      get_nama_veneer(id);
      clear_barang2();
    }   

    function get_nama_veneer(id_brg) {
      $.ajax({
        type    : 'POST',
        url     : '{{ route('get_veneer.json') }}',
        data    : { '_id' : id_brg },
        success : function (e) {
          console.log(e);
          $('#namaBrg').val(e);
        } 
      });
    }

    function clear_barang2() {
      $('#Qty').val('');
      $('#Keterangan').val('');
      $('#Subtotal').val('');
      $('#idSatuan').val('');
      $('#M3').val('');
    }

    var tb_barang = $('#tabel_brg').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            type: 'GET',
            url: '{{ route('bkVeneer.datatable_brg') }}', 
        },
        columns: [
          { data: 'DT_RowIndex', name: 'DT_RowIndex' },
          { data: 'nama_brg', name: 'nama_brg' },
          { data: 'veneer', name: 'veneer'},
          { data: 'jenis_brg', name: 'jenis_brg'},
          // { data: 'harga', name: 'harga' },
          // { data: 'satuan', name: 'satuan' },
          { data: 'opsi', name: 'opsi' },
        ]
      });

      function hitung_kubikasi(panjang, lebar, tebal, qty) {
        var hasil = (panjang * lebar * tebal * qty) / 10000000;
        $('#M3').val(hasil.toFixed(4));
      }

      function hitung_subtotal(harga, kubikasi) {
        var subtotal = ( parseInt(harga) * parseFloat(kubikasi));
        $('#Subtotal').val(subtotal);
        console.log(subtotal);
      }

      $(document).on('keyup', '.qtyQ', function(){
        var qty = $('#Qty').val();
        var harga = $('#Harga').val();

        var panjang = $('#Panjang').val();
        var lebar = $('#Lebar').val();
        tebal = $('#Tebal').val();
        hitung_kubikasi(panjang, lebar, tebal, qty);

        var kubikasi = $('#M3').val();
        hitung_subtotal(harga, kubikasi);
      });

      var tb_detail = '';
      tb_detail = $('#tb_detail').DataTable({
        processing: true,
        serverSide: true,
        lengthMenu: [[10, 25, 50, -1],[10, 25, 50, 'All']],
        ajax: {
            type: 'POST',
            url: '{{ route('bkVeneer.datatable_detail') }}', 
            data : function (e) {
              e._noBk = $('#noBk').val();
            }
        },
        columns: [
          { data: 'DT_RowIndex', name: 'DT_RowIndex' },
          { data: 'nama_brg', name: 'nama_brg' },
          { data: 'keterangan', name: 'keterangan' },
          { data: 'satuan', name: 'satuan' },
          { data: 'harga', name: 'harga' },
          { data: 'qty', name: 'qty' },
          { data: 'm3', name: 'm3' },
          { data: 'subtotal', name: 'subtotal', className:'text-right' },
          { data: 'opsi', name: 'opsi' },
        ],

        "footerCallback": function( tfoot, data, start, end, display ) {
          var api = this.api();
          var intVal = function ( i ) {
 
          return typeof i === 'string' ?
              i.replace(/[\$,]/g, '')*1 :
              typeof i === 'number' ?
                  i : 0;
                    
          };

          var total_qty = api
                .column( 5 )
                .data()
                .reduce( function (a, b) {
                  var total =  intVal(a) + intVal(b);
                  return total;
                }, 0 );


          var total_kubikasi = api
                .column( 6 )
                .data()
                .reduce( function (a, b) {
                  var total =  intVal(a) + intVal(b);
                  var total_format =  num_format(total.toFixed(4));
                  return total_format;
                }, 0 );

           var total = api
                .column( 7 )
                .data()
                .reduce( function (a, b) {
                  var total =  intVal(a) + intVal(b);
                  var total_format =  num_format2(total);
                  return total_format;
                }, 0 );

          $( api.column( 5 ).footer() ).html(total_qty);
          $( api.column( 6 ).footer() ).html(total_kubikasi);
          $( api.column( 7 ).footer() ).html(total);
        }
      });

      function delete_item(id) {
      $.ajax({
        type : 'POST',
        url : '{{ route('bkVeneer.delete_item') }}',
        data : { '_idItem' : id },
        success : function (e) {
          notif(e.response.code, e.response.msg);
          tb_detail.draw();
          }
        });
      }
  </script>
@endpush