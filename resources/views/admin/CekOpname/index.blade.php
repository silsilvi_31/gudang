@extends('master.ella')
    
@section('content')
<div class="right_col" role="main">
  <div class="">
    <div class="clearfix"></div>
    
    {{-- notif --}}
    @if (session('response'))
        @push('js')
          <script type="text/javascript">
            notif({{ session('response')['code'] }}, "{{ session('response')['msg'] }}");
          </script>
        @endpush
    @endif
    {{-- notif --}}

    <div class="row">
      <div class="col-md-12 col-sm-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Cek Opname</h2>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <div class="row">
              <div class="col-md-12">
                <div class="card-box table-responsive">
                  <table id="tb_cekopname" class="table table-sm table-striped table-bordered" style="width:100%">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>tgl</th>
                        <th>Total Opname</th>
                        <th>Total Cek</th>
                        {{-- <th>Nama Barang</th>
                        <th>Stok Sebelum</th>
                        <th>Stok Sesudah</th>
                        <th>Keterangan</th>
                        <th>Selisih</th> --}}
                        <th>Status</th>
                        <th>Opsi</th>
                      </tr>
                    </thead>
                    <tbody>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    
  </div>
</div>
        
@endsection

@push('js')
  <script type="text/javascript">
    var tb_cek_opname = $('#tb_cekopname').DataTable({
        processing: true,
        serverSide: true,
        ordering: false,
        ajax: {
            type: 'GET',
            url: '{{ route('cekopname.datatable') }}', 
        },
        columns: [
          
          { data: 'DT_RowIndex', name: 'DT_RowIndex' },
          { data: 'tgl', name: 'tgl' },
          { data: 'total_opname', name: 'total_opname' },
          { data: 'total_cek', name: 'total_cek' },
          // { data: 'nama_brg', name: 'nama_brg' },
          // { data: 'stok_sblm', name: 'stok_sblm' },
          // { data: 'stok_sesudah', name: 'stok_sesudah' },
          // { data: 'ketr', name: 'ketr' },
          // { data: 'selisih', name: 'selisih' },
          { data: 'is_cek_opname', name: 'is_cek_opname' },
          { data: 'opsi', name: 'opsi' }
        ]
    });

  </script>
@endpush