{{-- @extends('master.print') --}}
    
{{-- @section('content') --}}
<style>
    td .total {
        background-color : #f39c12 !important;
    }
</style>
<h3>Rekap Laba Rugi {{ date('d-m-Y', strtotime($data['tgl_m'])) }} - {{ date('d-m-Y', strtotime($data['tgl_a'])) }}</h3>
<br>

<div class="row">
    <div class="col-md-12">
        <div class="col-md-6">
            Pendapatan Laba Rugi
            <table>
                <tr>
                    <th>No Akun</th>
                    <th>Keterangan</th>
                    <th>Debit</th>
                    <th>Kredit</th>
                    <th>Total</th>
                </tr>
                @foreach ($data['rekap_pendapatan'] as $value)
                    <tr>
                        <td>{{ $value->no_akun_pendapatan }}</td>
                        <td>{{ $value->akun_pendapatan }}</td>
                        <td>{{ $value->debit_pendapatan }}</td>
                        <td>{{ $value->kredit_pendapatan}}</td>
                        <td>{{ $value->total_pendapatan}}</td>
                    </tr>
                @endforeach
                <tr>
                    <td colspan="2"><strong>Total</strong></td>
                    <td>{{ $data['tt_debit_pendapatan'] }}</td>
                    <td>{{ $data['tt_kredit_pendapatan'] }}</td>
                    <td class="total">{{ $data['tt_fix_pendapatan'] }}</td>
                </tr>
                @foreach ($data['rekap_hpp'] as $value)
                <tr>
                    <td>{{ $value->no_akun_hpp }}</td>
                    <td>{{ $value->akun_hpp }}</td>
                    <td>{{ $value->debit_hpp }}</td>
                    <td>{{ $value->kredit_hpp}}</td>
                    <td>{{ $value->total_hpp}}</td>
                </tr>
                @endforeach
                <tr>
                    <td colspan="2"><strong>Total</strong></td>
                    <td>{{ $data['tt_debit_hpp'] }}</td>
                    <td>{{ $data['tt_kredit_hpp'] }}</td>
                    <td class="total">{{ $data['tt_fix_hpp'] }}</td>
                </tr>
                @foreach ($data['rekap_beban'] as $value)
                <tr>
                    <td>{{ $value->no_akun_beban }}</td>
                    <td>{{ $value->akun_beban }}</td>
                    <td>{{ $value->debit_beban }}</td>
                    <td>{{ $value->kredit_beban}}</td>
                    <td>{{ $value->total_beban}}</td>
                </tr>
                @endforeach
                <tr>
                    <td colspan="2"><strong>Total</strong></td>
                    <td>{{ $data['tt_debit_beban'] }}</td>
                    <td>{{ $data['tt_kredit_beban'] }}</td>
                    <td class="total">{{ $data['tt_fix_beban'] }}</td>
                </tr>
                <tr>
                    <td colspan="4"> <strong>LABA/RUGI</strong></td>
                    <td>{{ $data['total_akhir_fix'] }}</td>
                </tr>
            </table>
            
        </div>
        
    </div>
</div>

{{-- @endsection --}}