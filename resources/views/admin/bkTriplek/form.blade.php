@extends('master.ella')
    
@section('content')
<div class="right_col" role="main">
  <div class="">
    <div class="clearfix"></div>
       <!-- Awal Modal Barang -->
    <div class="modal fade bs-example-modal-lg" id="modal_brg" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
              <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Data Barang</h4>
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                </button>
              </div>
              <div class="modal-body">
                <table class="table table-striped table-bordered" id="tabel_brg" style="width:100%">
                      <thead>
                          <tr>
                              <th>No.</th>
                              <th>Nama</th>
                              <th>Harga</th>
                              <th>Satuan</th>
                              <th>Opsi</th>
                          </tr>
                      </thead>
                      <tbody>
                      </tbody>
                  </table>
              </div>
          </div>
      </div>
    </div>
    <!-- Awal Modal barang -->

    <!-- ============================================================== -->
      <div class="row">
        <div class="col-md-12 col-sm-12">
          <div class="x_panel">
            <div class="x_title">
              <h2>Form BK Triplek</h2>
              <ul class="nav navbar-right panel_toolbox">
                <a href=" {{ route('bkTriplek.form') }} " class="btn btn-sm btn-success">New</a>
                <a href=" {{  route('bkTriplek.index') }} " class="btn btn-sm btn-secondary"><i class="fa fa-arrow-left"></i> Back </a>
              </ul>
              <div class="clearfix"></div>
            </div>

            <div class="x_content">
              <br/>
              <form id="" action="#" method="#" data-parsley-validate class="form-verical form-label-left">
                {{-- {{ csrf_field() }} --}}
                {{-- Kiri --}}
                <div class="col-md-6 col-sm-6">
                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">No BK <span class="required">*</span>
                    </label>
                    <div class="col-md-6 col-sm-6 ">
                      <input type="text" id="Form" name="form" required="required" class="form-control" value="{{ !empty($form) ? $form : ''}}" readonly>
                      <input type="text" id="noBk" name="no_bk" required="required" class="form-control" readonly value="{{ !empty($no_bk) ? $no_bk : ''}}">
                    </div>
                  </div>
                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Tanggal
                    </label>
                    <div class="col-md-6 col-sm-6 ">
                      <input type="text" id="Tgl" name="tgl" required='required' class="form-control" value="{{ !empty($tgl) ? $tgl : ''}}" >
                    </div>
                  </div>
                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Kepada <span class="required">*</span>
                    </label>
                    <div class="col-md-6">
                      @php
                          $kepadaQ = !empty($id_kepada) ? $id_kepada : '';
                      @endphp
                      <select class="form-control" id="idKepada" name="id_kepada" autocomplete="off" required='required'>
                        <option value="" id="kepada_kosong" >-</option>
                        @foreach ($kepada as $item)
                          <option value="{{ $item->id }}" @if($item->id == $kepadaQ) selected @endif>{{ $item->kepada }}</option>                              
                          {{-- <option value="{{ $item->id }}" >{{ $item->kepada }}</option>                               --}}
                        @endforeach
                      </select>
                    </div>
                  </div>
                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Ketr <span class="required">*</span>
                    </label>
                    <div class="col-md-6 col-sm-6 ">
                      <input type="text" id="Ketr" name="ketr" required="required" class="form-control" placeholder="ex : ngelontok" autocomplete="off" value="{{ !empty($ketr) ? $ketr : '' }}">
                    </div>
                  </div>
                </div>

                <div class="clearfix"></div>
                <div class="ln_solid"></div>
                {{-- kiri --}}
                <div class="col-md-6 col-sm-6">
                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Nama Barang
                    </label>
                    <div class="col-md-6 col-sm-6">
                      <input type="text" id="idItem" name="id_item" class="form-control" autocomplete="off" readonly>
                      <input type="text" id="idBrg" name="id_brg" class="form-control" autocomplete="off" readonly>
                      <input type="text" id="namaBrg" name="nama_brg" class="form-control brgQ" autocomplete="off">
                    </div>
                  </div>
                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name"> Satuan
                    </label>
                    <div class="col-md-6 col-sm-6 ">
                      <select id="idSatuan"  name="id_satuan" class="form-control" autocomplete="off">
                        <option value="">-</option>
                        @foreach ($satuan as $item)
                          <option value="{{ $item->id }}">{{ $item->nama }}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                </div>  

                {{-- kanan --}}
                <div class="col-md-6 col-sm-6">
                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Qty
                    </label>
                    <div class="col-md-6 col-sm-6 ">
                      <input type="text" id="Qty" name="qty" class="form-control qtyQ" autocomplete="off">
                    </div>
                  </div>
                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Harga
                    </label>
                    <div class="col-md-6 col-sm-6 ">
                      <input type="text" id="Harga" name="harga" class="form-control" autocomplete="off" readonly>
                    </div>
                  </div>
                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Subtotal
                    </label>
                    <div class="col-md-6 col-sm-6 ">
                      <input type="text" id="Subtotal" name="subtotal" class="form-control" autocomplete="off" readonly>
                    </div>
                  </div>
                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Keterangan
                    </label>
                    <div class="col-md-6 col-sm-6 ">
                      <input type="text" id="Keterangan" name="keterangan" class="form-control" autocomplete="off">
                    </div>
                  </div>
                </div>

                <div class="clearfix"></div>
                <div class="ln_solid"></div>
                <div class="col-md-6 col-sm-6">
                  <div class="form-group">
                    <div class="offset-md-3">
                      {{-- <button type="button" class="btn btn-primary">Cancel</button> --}}
                      <button class="btn btn-sm btn-primary" type="reset">Reset</button>
                      <button type="button" class="btn btn-sm btn-success" onclick="save()">Submit</button>
                    </div>
                  </div>
                </div>
              </div>                                   
              </form>

              <div class="clearfix"></div>
                <div class="ln_solid"></div>
                <table class="table table-striped table-bordered" id="tb_detail" style="width:100%">
                  <thead>
                      <tr>
                          <th>No.</th>
                          <th>Nama Barang</th>
                          <th>Keterangan</th>
                          <th>Satuan</th>
                          <th>Harga</th>
                          <th>Qty</th>
                          <th>Subtotal</th>
                          <th>Opsi</th>
                      </tr>
                  </thead>
                  <tbody>
                  </tbody>
                  <tfoot>
                    <td colspan="5" style="text-align:right"> <strong>Total:</strong> </td>
                    <td></td>
                    <td></td>
                  </tfoot>
              </table>

              <div class="clearfix"></div>
              <div class="ln_solid"></div>
              </div>
          </div>
        </div>
      </div>
  </div>
</div>        
@endsection

@push('js')
  <script type="text/javascript">
    $('#Tgl').datetimepicker({
        format: 'DD-MM-YYYY'
    });

    var form = $('#Form').val();
    if (form == 'edit') {

    } else {
      no_urut();
    }

    function no_urut() {
      $.ajax({
              type : 'GET',
              url : '{{ route('no_bk') }}',
              success : function (e) {
                console.log(e);
                $('#noBk').val(e);
              }
      });
    }

    function num_format(nStr)
    {
        nStr += '';
        x = nStr.split('.');
        x1 = x[0];
        x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + '.' + '$2');
        }
        return x1 + x2;
    }

    function save() {
      var no_bk = $('#noBk').val();
      var id_kepada = $('#idKepada').val();
      var tgl = $('#Tgl').val();
      var ketr = $('#Ketr').val();
      
      var id_item = $('#idItem').val();
      var id_brg = $('#idBrg').val();
      var nama_brg = $('#namaBrg').val();
      var id_satuan = $('#idSatuan').val();
      var qty = $('#Qty').val();
      var harga = $('#Harga').val();
      var subtotal = $('#Subtotal').val();
      var keterangan = $('#Keterangan').val();

      $.ajax({
        type  : 'POST',
        url   : '{{ route('bkTriplek.save') }}',
        data  : {
                  '_noBk' : no_bk,
                  '_idKepada' : id_kepada,
                  '_tgl' : tgl,
                  '_ketr' : ketr,
                  '_idItem' : id_item,
                  '_idBrg' : id_brg,
                  '_namaBrg' : nama_brg,
                  '_idSatuan' : id_satuan,
                  '_qty' : qty,
                  '_harga' : harga,
                  '_subtotal' : subtotal,
                  '_keterangan' : keterangan
                },
        success  : function (e) {
          tb_detail.draw();
          notif(e.code,e.msg);
          clear_akun();
        }
      });
    }

    function clear_akun() {
      $('#idBrg').val('');
      $('#namaBrg').val('');
      $('#idSatuan').val('');
      $('#Qty').val('');
      $('#Harga').val('');
      $('#Subtotal').val('');
      $('#Keterangan').val('');
    }

    $(document).on('click', '.brgQ', function(){  
      $('#modal_brg').modal('show');  
    });

    function select_brg(kode_brg,nama_brg,harga, id_satuan, satuan) {
      $('#modal_brg').modal('hide');
      $('#idBrg').val(kode_brg);
      $('#namaBrg').val(nama_brg);
      $('#Harga').val(harga);
      $('#idSatuan').val(id_satuan);
      $('#Satuan').val(satuan);
      clear_barang2();
    }   

    function clear_barang2() {
      $('#Qty').val('');
      $('#Keterangan').val('');
      $('#Subtotal').val('');
      $('#idSatuan').val('');
    }

    var tb_barang = $('#tabel_brg').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            type: 'GET',
            url: '{{ route('bkTriplek.datatable_brg') }}', 
        },
        columns: [
          { data: 'DT_RowIndex', name: 'DT_RowIndex' },
          { data: 'nama_brg', name: 'nama_brg' },
          { data: 'harga', name: 'harga' },
          { data: 'satuan', name: 'satuan' },
          { data: 'opsi', name: 'opsi' },
        ]
      });

      function hitung_subtotal(harga, qty) {
        var subtotal = ( harga * qty );
        $('#Subtotal').val(subtotal);
      }

      $(document).on('keyup', '.qtyQ', function(){
        var qty = $('#Qty').val();
        var harga = $('#Harga').val();
        hitung_subtotal(harga, qty);
      });

      var tb_detail = '';
      tb_detail = $('#tb_detail').DataTable({
        processing: true,
        serverSide: true,
        lengthMenu: [[10, 25, 50, -1],[10, 25, 50, 'All']],
        ajax: {
            type: 'POST',
            url: '{{ route('bkTriplek.datatable_detail') }}', 
            data : function (e) {
              e._noBk = $('#noBk').val();
            }
        },
        columns: [
          { data: 'DT_RowIndex', name: 'DT_RowIndex' },
          { data: 'nama_brg', name: 'nama_brg' },
          { data: 'keterangan', name: 'keterangan' },
          { data: 'satuan', name: 'satuan' },
          { data: 'harga', name: 'harga' },
          { data: 'qty', name: 'qty' },
          { data: 'subtotal', name: 'subtotal', className:'text-right' },
          { data: 'opsi', name: 'opsi' },
        ],

        "footerCallback": function( tfoot, data, start, end, display ) {
          var api = this.api();
          var intVal = function ( i ) {
 
          return typeof i === 'string' ?
              i.replace(/[\$,.]/g, '')*1 :
              typeof i === 'number' ?
                  i : 0;
                    
          };

          var total_qty = api
                .column( 5 )
                .data()
                .reduce( function (a, b) {
                  var total =  intVal(a) + intVal(b);
                  return total;
                }, 0 );


          var total = api
                .column( 6 )
                .data()
                .reduce( function (a, b) {
                  var total =  intVal(a) + intVal(b);
                  var total_format =  num_format(total);
                  return total_format;
                }, 0 );

          $( api.column( 5 ).footer() ).html(total_qty);
          $( api.column( 6 ).footer() ).html(total);
        }
      });

      function delete_item(id) {
      $.ajax({
        type : 'POST',
        url : '{{ route('bkTriplek.delete_item') }}',
        data : { '_idItem' : id },
        success : function (e) {
          notif(e.response.code, e.response.msg);
          tb_detail.draw();
          }
        });
      }
  </script>
@endpush