@extends('master.ella')
    
@section('content')
<div class="right_col" role="main">
  <div class="">
    <div class="clearfix"></div>

    <!-- Awal Modal Suratjalan -->
    <div class="modal fade bs-example-modal-lg" id="modal_sj" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
              <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Data Suratjalan</h4>
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                </button>
              </div>
              <div class="modal-body table-responsive">
                <table class="table table-striped table-bordered" id="tabel_sj" style="width:100%">
                      <thead>
                          <tr>
                              <th>No.</th>
                              <th>Tanggal</th>
                              <th>ID Sj</th>
                              <th>Pelanggan</th>
                              <th>Tagihan</th>
                              <th>DiBayar</th>
                              <th>Sisa</th>
                              <th>Opsi</th>
                          </tr>
                      </thead>
                      <tbody>
                      </tbody>
                  </table>
              </div>
          </div>
      </div>
    </div>
    <!-- Awal Modal Suratjalan -->


    <!-- ============================================================== -->
      <div class="row">
        <div class="col-md-12 col-sm-12">
          <div class="x_panel">
            <div class="x_title">
              <h2>Pelunasan</h2>
              <ul class="nav navbar-right panel_toolbox">
                <a href=" {{  route('transaksi.index') }} " class="btn btn-sm btn-secondary"><i class="fa fa-arrow-left"></i> Back </a>
              </ul>
              <div class="clearfix"></div>
            </div>

            <div class="x_content">
              <br/>
              <form id="" action="#" method="#" data-parsley-validate class="form-verical form-label-left">
                {{-- {{ csrf_field() }} --}}
                {{-- Kiri --}}
                <div class="col-md-6 col-sm-6">
                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">No. Nota <span class="required">*</span>
                    </label>
                    <div class="col-md-6 col-sm-6 ">
                      <input type="text" id="idSj" name="id_sj" class="form-control id_sjQ" autocomplete="off">
                    </div>
                  </div>

                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Tanggal
                    </label>
                    <div class="col-md-6 col-sm-6 ">
                      <input type="text" id="Tgl" name="tgl" required='required' class="form-control" autocomplete="off">
                    </div>
                  </div>

                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Pembayaran <span class="required">*</span>
                    </label>
                    <div class="col-md-6 col-sm-6 ">
                      <select class="form-control" id="jenisPembayaran" name="pembayaran" autocomplete="off" required='required'>
                        <option></option>
                        <option>Tunai</option>
                        <option>Transfer</option>
                      </select>
                    </div>
                  </div>

                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Sisa
                    </label>
                    <div class="col-md-6 col-sm-6 ">
                      <input type="text" id="Sisa" name="sisa" class="form-control" autocomplete="off" readonly>
                    </div>
                  </div>

                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Total
                    </label>
                    <div class="col-md-6 col-sm-6 ">
                      <input type="text" id="Total" name="total" class="form-control" autocomplete="off">
                    </div>
                  </div>

                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Keterangan
                    </label>
                    <div class="col-md-6 col-sm-6 ">
                      <input type="text" id="Ketr" name="ketr" class="form-control" autocomplete="off">
                    </div>
                  </div>
                </div>

                <div class="clearfix"></div>
                <div class="ln_solid"></div>
                <div class="col-md-6 col-sm-6">
                  <div class="form-group">
                    <div class="offset-md-3">
                      <button class="btn btn-sm btn-primary" type="reset">Reset</button>
                      <button type="button" class="btn btn-sm btn-success" onclick="simpan_transaksi()">Submit</button>
                    </div>
                  </div>
                </div>
              </div>                                   
              </form>

              <div class="clearfix"></div>
              <div class="ln_solid"></div>
              </div>
          </div>
        </div>
      </div>
  </div>
</div>        
@endsection

@push('js')
  <script type="text/javascript">
  $('#Tgl').datetimepicker({
      format: 'DD-MM-YYYY'
  });

  $(document).on('click', '.id_sjQ', function(){  
    $('#modal_sj').modal('show');  
  });

  var tb_sj = $('#tabel_sj').DataTable({
      processing: true,
      serverSide: true,
      ajax: {
          type: 'GET',
          url: '{{ route('transaksi.datatable_sj') }}', 
      },
      columns: [
        { data: 'DT_RowIndex', name: 'DT_RowIndex' },
        { data: 'tgl',name: 'tgl'},
        { data: 'id_sj', name: 'id_sj' },
        { data: 'nama', name: 'nama' },
        { data: 'total', name: 'total' },
        { data: 'bayar', name: 'bayar' },
        { data: 'sisa', name: 'sisa' },
        { data: 'opsi', name: 'opsi' },
      ]
    });

    function select_sj(id_sj, sisa) {
    $('#modal_sj').modal('hide');
    $('#idSj').val(id_sj);
    $('#Sisa').val(sisa);
  }

  function simpan_transaksi() {
    var id_sj = $('#idSj').val();
    var tgl = $('#Tgl').val();
    var jenis_byr = $('#jenisPembayaran').val();
    var total = $('#Total').val();
    var ketr = $('#Ketr').val();

    $.ajax({
      type    : 'POST',
      url     : '{{ route('transaksi.save') }}',
      data    : {
                '_idSj'     : id_sj,
                '_tgl'      : tgl,
                '_jenisByr' : jenis_byr,
                '_total'    : total,
                '_ketr'     : ketr
      },
      success : function (e) {
        notif(e.response.code, e.response.msg);
        if (e.response.code == 200) {
          window.location.href = '{{ route('transaksi.index') }}';
        }
      } 
    });
  }  
      

  </script>
@endpush