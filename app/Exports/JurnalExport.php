<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class JurnalExport implements FromView
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function __construct($data)
    {
        $this->data = $data;
    } 

    public function view(): View
    {
        // dd($this->data);
       return view('admin.jurnal.excel', ['data' => $this->data]);
    }
}
