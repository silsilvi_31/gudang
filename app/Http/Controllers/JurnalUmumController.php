<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\JurnalUmumDuaController;
use Yajra\Datatables\Datatables;
use Session;
use DB;
use Redirect;
use Validator;
use File;
use Excel;
use App\Exports\SjExport;
use Exception;

class JurnalUmumController extends Controller
{
    public function __construct()
    {
        date_default_timezone_set("Asia/Jakarta");
    }

    public function index()
    {
        return view('admin.jurnalUmum.index');
    }

    public function form()
    {
        $satuan = DB::table('satuan')->where('status', null)->get();
        $jenis_brg = DB::table('jenis_barang')->where('status', null)->get();
        $akun= DB::table('akun')->where('status', null)->get();

        $data['satuan'] = $satuan;
        $data['jenis_barang'] = $jenis_brg;
        $data['akun'] = $akun;
        return view('admin.jurnalUmum.form')->with($data);
    }

    public function form_edit($id_ju)
    {
        $id_ju = base64_decode($id_ju);
        $jurnal_umum = DB::table('jurnal_umum as a')
                            ->leftJoin('akun as b', 'a.no_akun', '=', 'b.no_akun')    
                            ->where('id_ju', $id_ju)
                            ->first();

        $satuan = DB::table('satuan')->where('status', null)->get();
        $jenis_brg = DB::table('jenis_barang')->where('status', null)->get();
        $akun= DB::table('akun')->where('status', null)->get();

        $data['id_ju'] = $jurnal_umum->id_ju;
        $data['tgl'] = date('d-m-Y', strtotime($jurnal_umum->tgl));
        $data['nama'] = $jurnal_umum->nama;
        $data['no_akun'] = $jurnal_umum->no_akun;
        $data['nama_akun'] = $jurnal_umum->akun;
        $data['form'] = 'edit';
        $data['satuan'] = $satuan;
        $data['jenis_barang'] = $jenis_brg;
        $data['akun'] = $akun;

        return view('admin.jurnalUmum.form')->with($data);
    }

    public function no_urut()
    {
        $id_jurnalUmum = DB::table('jurnal_umum')->where('status',NULL)->max('id_ju');
        $no = $id_jurnalUmum;
        $no++;
        return response()->json($no);
    }

    public function set_akun($jurnal, $detail_jurnal)
    {
        $akun[0]['tgl'] = $jurnal['tgl'];
        $akun[0]['id_item'] = null;
        $akun[0]['no_akun'] = $jurnal['no_akun'];
        $akun[0]['jenis_jurnal'] = 'ju';
        $akun[0]['ref'] = strtolower($jurnal['id_ju']);
        $akun[0]['nama'] = $jurnal['nama'];
        $akun[0]['keterangan'] = $detail_jurnal['ketr'];
        $akun[0]['map'] = 'd';
        $akun[0]['hit'] = null;
        $akun[0]['grup'] = 1;
        $akun[0]['qty'] = $detail_jurnal['qty'];
        $akun[0]['harga'] = $detail_jurnal['harga'];
        $akun[0]['total'] = $detail_jurnal['subtotal'];

        $akun[1]['tgl'] = $jurnal['tgl'];
        $akun[1]['id_item'] = null;
        $akun[1]['no_akun'] = $detail_jurnal['no_akun_brg'];
        $akun[1]['jenis_jurnal'] = 'ju';
        $akun[1]['ref'] = strtolower($jurnal['id_ju']);
        $akun[1]['nama'] = $jurnal['nama'];
        $akun[1]['keterangan'] = $detail_jurnal['ketr'];
        $akun[1]['map'] = 'k';
        $akun[1]['hit'] = null;
        $akun[1]['grup'] = 2;
        $akun[1]['qty'] = $detail_jurnal['qty'];
        $akun[1]['harga'] =  $detail_jurnal['harga'];
        $akun[1]['total'] = $detail_jurnal['subtotal'];

        $insert = DB::table('jurnal')->insert($akun);
    }

    public function update_jurnal($id_ju, $nama, $tgl)
    {
        $update_jl = DB::table('jurnal')->where('ref', $id_ju)->where('jenis_jurnal', 'ju')->update([
            'nama' => $nama,
            'tgl'  => $tgl
        ]);
    }

    public function save(Request $req)
    {
        $id_user = session::get('id_user');
        $no_jurnal_umum = $req->_noJurnalUmum;
        $tgl = date("Y-m-d", strtotime($req->_tgl)) ;
        $nama = $req->_nama;
        $no_akun_debit = $req->_noAkunDebit;

        $id_brg = $req->_idBrg;
        $nama_brg = $req->_namaBrg;
        $no_akun_kredit = $req->_noAkunKredit;
        $id_satuan = $req->_idSatuan;
        $qty = $req->_qty;
        $harga = $req->_harga;
        $subtotal = $req->_subtotal;
        $ketr = $req->_ketr;

        $data_jurnal = [
            'id_ju'         => $no_jurnal_umum,
            'tgl'           => $tgl,
            'nama'          => $nama,
            'no_akun'       => $no_akun_debit,
            'user_add'      => $id_user,
            'created_at'    => date('Y-m-d H:i:s')
        ];

        $data_detail_jurnal = [
            'id_ju'         => $no_jurnal_umum,
            'id_brg'        => $id_brg,
            'nama_brg'      => $nama_brg,
            'no_akun_brg'   => $no_akun_kredit,
            'id_satuan'     => $id_satuan,
            'qty'           => $qty,
            'harga'         => $harga,
            'subtotal'      => $subtotal,
            'ketr'          => $ketr
        ];

        $cek_ju = DB::table('jurnal_umum')->where('id_ju', $no_jurnal_umum)->first();

        if (!$tgl || !$nama || !$no_akun_debit) {
            $res = [
                'code' => 300,
                'msg' => 'Data Belum diisi lengkap'
            ];
        } else {
            if (is_null($cek_ju)) {
                $insert_ju = DB::table('jurnal_umum')->insert($data_jurnal);
                if ($insert_ju) {
                    $insert_ju_detail = DB::table('jurnal_umum_detail')->insertGetId($data_detail_jurnal);
                    $jumlah = DB::table('jurnal_umum_detail')->where('id_ju',$no_jurnal_umum)->where('status',NULL)->sum('subtotal');
                    $update_total_ju = DB::table('jurnal_umum')->where('id_ju', $no_jurnal_umum)->update([
                        'total' => $jumlah
                    ]);
                    $this->set_akun($data_jurnal, $data_detail_jurnal);
                    $res = [
                        'code' => 200,
                        'msg' => 'Data Berhasil disimpan'
                    ];
                } else {
                    $res = [
                        'code' => 400,
                        'msg' => 'Data Gagal disimpan'
                    ];
                }
            } elseif (!is_null($cek_ju)) {
                $update_ju = DB::table('jurnal_umum')->where('id_ju', $no_jurnal_umum)->update($data_jurnal);

                if ($update_ju) {
                    $data_ju = DB::table('jurnal_umum')->where('id_ju', $no_jurnal_umum)->first();
                    $cek_count = DB::table('jurnal_umum_detail')->where('id_ju', $no_jurnal_umum)->count('id');

                    $res = [
                        'code' => 200,
                        'msg' => 'Berhasil Diupdate',
                    ];

                    if ($cek_count == 0) {
                        $insert_ju_detail = DB::table('jurnal_umum_detail')->insertGetId($data_detail_jurnal);
                        $jumlah = DB::table('jurnal_umum_detail')->where('id_ju',$no_jurnal_umum)->where('status',NULL)->sum('subtotal');
                        $update_total_ju = DB::table('jurnal_umum')->where('id_ju', $no_jurnal_umum)->update([
                            'total' => $jumlah
                        ]);
                        $this->set_akun($data_jurnal, $data_detail_jurnal);
                    } elseif ( (isset($id_brg) && isset($cek_count) ) ) { 
                        $res = [
                            'code' => 300,
                            'msg' => 'Hanya Bisa input 1 transaksi',
                        ];
                    }
                    $this->update_jurnal($no_jurnal_umum, $nama, $tgl);
                // $this->update()
                } else {
                    $res = [
                        'code' => 400,
                        'msg' => 'Data Gagal disimpan'
                    ];
                }
            }

        }
        return response()->json($res);
    }

    public function get_karyawan($id_user)
    {
        $data = DB::table('karyawan')->where('id_users', $id_user)->first();
        return $data->nama;
    }

    public function datatable()
    {
        $parent_jurnal = DB::table('parent_jurnal')
                                    ->where('status', 'tutup')
                                    ->orderBy('created_at', 'DESC')
                                    ->first();
        
        $tgl_akhir = isset($parent_jurnal) ? $parent_jurnal->tgl_akhir : '';

        $jurnal = DB::table('jurnal_umum')
                        ->whereDate('tgl', '<', $tgl_akhir)
                        ->get();
        $id_ju = [];
        
        foreach ($jurnal as $value) {
            $id_ju[] = $value->id;
        }

        $data = DB::table('jurnal_umum as a')
                        ->leftJoin('jurnal_umum_detail as b', 'a.id_ju', '=', 'b.id_ju')
                        ->select('a.id_ju', 'a.tgl','a.nama', 'a.total', 'a.user_add', 'a.is_cek_jurnal', 'a.cek_jurnal', 'b.id_brg')
                        ->orderBy('a.tgl', 'DESC')
                        ->whereNotIn('id', $id_ju)
                        ->get(); 

                            
        return datatables::of($data)
        ->addIndexColumn()
        ->editColumn('tgl', function ($data) {
            $tanggal = date('d-m-Y', strtotime($data->tgl));
            return $tanggal;
        }) 
        ->editColumn('is_cek_jurnal', function ($data) {
            $is_cek_jurnal = $data->is_cek_jurnal;
            $nama = isset($is_cek_jurnal) ? $this->get_karyawan($data->cek_jurnal) : '-';
            
            if ($is_cek_jurnal == 1) {
                return '<i class="fa fa-check"></i>'.$nama;
            } else if ($is_cek_jurnal == 2) {
                return '<i class="fa fa-close"></i>'.$nama;
            } else if ($is_cek_jurnal == '') {
                return '<i class="fa fa-minus">';
            } else {
            
            }
        }) 
        ->addColumn('opsi', function ($data){
            $JurnalUmumDuaController = new JurnalUmumDuaController;
            $edit = route('jurnalUmum.form_edit', [base64_encode($data->id_ju)]);
            $edit_ju = route('jurnalUmumDua.form_edit', [base64_encode($data->id_ju)]);
            $edit_fix = isset($data->id_brg) ? $edit : $edit_ju;

            $id = $data->id_ju;
            $status_hapus = isset($data->is_cek_jurnal) ? 'disabled' : '';
            $status_edit = isset($data->is_cek_jurnal) ? 'disabled' : '';
            return '<a href="'.$edit_fix.'" class="btn btn-sm btn-primary '.$status_edit.'"><i class="fa fa-edit"></i><a/>
                    <button type="button" class="btn btn-sm btn-danger" '.$status_hapus.' onclick="delete_jurnalUmum('.$id.')"><i class="fa fa-trash"></i></button>';
        })
        ->rawColumns(['tgl', 'opsi', 'is_cek_jurnal'])
        ->make(true);
    }

    public function datatable_detail(Request $req)
    {
        $id_ju = $req->_noJu;
        $data = DB::table('jurnal_umum_detail as a')
                        ->leftJoin('satuan as b', 'a.id_satuan', '=', 'b.id')
                        ->where('id_ju', $id_ju)
                        ->select('a.id', 'a.id_ju', 'a.id_brg', 'a.nama_brg', 'a.harga', 'a.qty', 'a.ketr', 'b.nama')
                        ->get();

        return datatables::of($data) 
        ->addIndexColumn()
        ->addColumn('opsi', function ($data) {
            return '<button type="button" class="btn btn-sm btn-danger" onclick="delete_item('.$data->id_ju.','.$data->id.')"><i class="fa fa-trash"></i></button>';
        })
        ->rawColumns(['opsi'])
        ->make(true);
    }

    public function datatable_akun_debit()
    {
        $data = DB::table('akun')->get();

        return datatables::of($data)
        ->addIndexColumn()
        ->addColumn('opsi', function ($data){
            $no_akun = "'".$data->no_akun."'";
            $akun = "'".$data->akun."'";
            return '<button id="btn_pilih" type="button" class="btn btn-sm btn-primary" onclick="select_akun('.$no_akun.','.$akun.')">Pilih</button>';
         })
         ->rawColumns(['opsi'])
         ->make(true);
    }

    public function datatable_akun_kredit()
    {
        $data = DB::table('akun')->get();

        return datatables::of($data)
        ->addIndexColumn()
        ->addColumn('opsi', function ($data){
            $no_akun = "'".$data->no_akun."'";
            $akun = "'".$data->akun."'";
            return '<button id="btn_pilih" type="button" class="btn btn-sm btn-primary" onclick="select_akun_kredit('.$no_akun.','.$akun.')">Pilih</button>';
         })
         ->rawColumns(['opsi'])
         ->make(true);
    }

    public function datatable_brg()
    {
        $data = DB::table('barang as a')
                ->where('a.status',NULL)
                ->leftJoin('satuan as b', 'a.satuan', '=', 'b.id')
                ->select('a.kode', 'a.nama_brg', 'a.harga', 'a.satuan as id_satuan','b.nama as satuan', 'a.jenis_brg','a.stok','a.tgl_opname','a.no_akun')
                ->get();
        
        return Datatables::of($data)
        ->addIndexColumn()
        ->addColumn('opsi', function ($data){
            $kode_barang = $data->kode;
            $nama_brg = "'".$data->nama_brg."'";
            $harga = $data->harga;
            $id_satuan = $data->id_satuan;
            $satuan = "'".$data->satuan."'";
            $no_akun = "'".$data->no_akun."'";
            return '<button id="btn_pilih" type="button" class="btn btn-sm btn-primary" onclick="select_brg('.$kode_barang.','.$nama_brg.','.$harga.','.$id_satuan.','.$satuan.','.$no_akun.')">Pilih</button>';
            return 'opsi';
        })
        ->rawColumns(['opsi'])
        ->make(true);
    }

    public function add_brg(Request $req)
    {
        $id_user = session::get('id_user');

        $nama_brg = $req->_namaBrg;
        $harga = $req->_harga;
        $jenis_brg = $req->_jenisBrg;
        $id_satuan = $req->_idSatuan;
        $no_akun = $req->_noAkun;

        $data_barang = [
            'nama_brg' => $nama_brg,
            'harga' => $harga,
            'jenis_brg' => $jenis_brg,
            'satuan' => $id_satuan,
            'no_akun' => $no_akun
        ];

        $return_id = DB::table('barang')->insertGetId($data_barang);

        $help_a = substr($no_akun, 0, 1);
        $help_b = substr($no_akun, 3, 1);

        $data_setAkun = [
            'kode' => $no_akun,
            'id_set' => $return_id,
            'help_a' => $help_a,
            'help_b' => $help_b,
            'help_c' => 'beli'
        ];

        if(isset($return_id)){
            // $insert_set_akun = DB::table('set_akun')->insert($data_setAkun);
            $res = [
                'code' => 200,
                'msg' => 'Barang berhasil ditambahkan',
                'kode' => $return_id,
                'nama_brg' => $nama_brg,
                'harga' => $harga,
                'jenis_brg' => $jenis_brg,
                'id_satuan' => $id_satuan,
                'no_akun' => $no_akun,
                'created_at' => date("Y-m-d H:i:s"),
                'user_add' => $id_user

            ];
        }else{
            $res = [
                'code' => 400,
                'msg' => 'Kepada Gagal ditambahkan !',
                'kode' => NULL,
                'nama_brg' => NULL,
                'harga' => NULL,      
                'jenis_brg' => NULL,
                'id_satuan' => NULL,
                'no_akun' => NULL,
                'created_at' => NULL,
                'user_add' => NULL
            ];
        }
        return response()->json($res);
    }

    public function delete(Request $req)
    {
        $id = $req->_id;

        DB::beginTransaction();

        try {
            $delete = DB::table('jurnal_umum')->where('id_ju', $id)->delete();
            $delte_detail = DB::table('jurnal_umum_detail')->where('id_ju', $id)->delete();
            $delete = DB::table('jurnal')->where('jenis_jurnal', 'ju')->where('ref', $id)->delete();

            DB::commit();
            $res = [
                'code'  => '300',
                'msg'   => 'Data berhasil dihapus'
            ];
        } catch (Exception $th) {
            DB::rollback();
            $res = [
                'code'  => '400',
                'msg'   => 'Data Gagal dihapus'
            ];
        }

        $data['response'] = $res;
        return response()->json($data);
    }

    public function delete_item(Request $req)
    {
        $id_user = session::get('id_user');
        $id_ju = $req->_idJu;
        $id_item = $req->_idItem;

        $data_delete = [
            'updated_at'    => date('Y-m-d H:i:s'),
            'user_upd'      => $id_user,
            'status'        => $id_user
        ];

        $res = [];

        DB::beginTransaction();
        try {
            $detele = DB::table('jurnal_umum_detail')->where('id', $id_item)->delete();
            $delete_jurnal = DB::table('jurnal')->where('ref', $id_ju)->where('jenis_jurnal', 'ju')->delete();
            DB::commit();
            $res = [
                'code' => 300,
                'msg' => 'Data telah dihapus'
            ];
        } catch (Exception $th) {
            DB::rollback();
            $res = [
                'code' => 400,
                'msg' => 'Gagal dihapus'
            ];
        }

        $data['response'] = $res;
        return response()->json($data);
    }

}
