<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Validator;
use Hash;

class RestApiController extends Controller
{
    public function data_presensi()
    {
        $respon = [];
        $respon = [
                    'code' => 200,
                    'msg' => "success"];
        return response()->json($respon);
    }

    // Android
    public function login_android(Request $req)
    {
        $karyawan = [];
        $username = $req->username;
        $password = $req->password;
        $kd_karyawan = $req->kd_karyawan;
        
        if (!empty($username) && !empty($password)) {
            $user = DB::table('users')
                    ->where('username', $username)
                    ->first();

            if(!empty($user)) {
                if (Hash::check($password, $user->password)) {
                    $karyawan = DB::table('karyawan')
                    ->where('id_user', $user->id)
                    ->select('kd_karyawan', 'nama', 'id_user')
                    ->first();
                }else {
                    $msg = (object)[
                            'message' => 'Password salah'
                        ];
                    $data['data'] = $msg;
                }
            }else {
                $msg = (object)[
                        'message' => 'Username tidak terdaftar'
                    ];
                $data['data'] = $msg;
            }        
        }elseif (!empty($kd_karyawan)) {
            $karyawan = DB::table('karyawan')
            ->where('kd_karyawan', $kd_karyawan)
            ->select('kd_karyawan', 'nama', 'id_user')
            ->first();
        }

        $data = [];
        if (!is_null($karyawan)) {
            $data['data'] = $karyawan;

        }else {
            $msg = (object)[
                    'message' => 'Gagal login'
                ];
            $data['data'] = $msg;
        }
        return response()->json($data, 200);
    }

    public function absen_masuk(Request $req)
    {
        date_default_timezone_set("Asia/Jakarta");
        $notif = [];
        $absen_masuk = [];
        $tgl = date("Y-m-d");
        $jam = date("H:i:s");
        
        $id_user = $req->id_user;
        $kd_karyawan = $req->kd_karyawan;
        $latlong = $req->latlong;
        // $data_karyawan = DB::table('karyawan as a')
        //                     ->where('a.id_user', $id_user)
        //                     ->leftJoin('posisi_karyawan as c', 'a.id_posisi_karyawan', 'c.id')
        //                     ->select('a.kd_karyawan', 'a.nama', 'a.id_shift', 'c.posisi')
        //                     ->first();
        $data_karyawan = DB::table('karyawan as a')
                            ->where('a.kd_karyawan', $kd_karyawan)
                            ->leftJoin('posisi_karyawan as c', 'a.id_posisi_karyawan', 'c.id')
                            ->select('a.kd_karyawan', 'a.nama', 'a.id_shift', 'c.posisi')
                            ->first();

        // if (empty($id_user)) {
        //     return redirect()->route('login');
        // }

        $cek_presensi_masuk = DB::table('presensi')
                                ->where('tgl_masuk', $tgl)
                                ->where('kd_karyawan', $data_karyawan->kd_karyawan)
                                ->first();

        if (empty($cek_presensi_masuk)) {
            $absen_masuk = [
                            'kd_karyawan' => $data_karyawan->kd_karyawan,
                            'shift' => 'P1',
                            'tgl_masuk' => $tgl,
                            'jam_masuk' => $jam,
                            'latlong_masuk' => $latlong,
                            'created_at' => date("Y-m-d H:i:s")
                        ];

            $insert_absen_masuk = DB::table('presensi')->insert($absen_masuk);
            if ($insert_absen_masuk) {
                $notif = (object)[
                    'code' => 200,
                    'message' => 'Berhasil absen masuk'
                ];
            }
        }else {
            $notif = (object)[
                        'code' => 400,
                        'message' => 'Anda sudah absen masuk !'
                    ];
        }
        $data['data'] = $notif;

        return response()->json($data);
    }

    public function absen_pulang(Request $req)
    {
        date_default_timezone_set("Asia/Jakarta");
        $notif = [];
        $absen_pulang = [];
        $tgl = date("Y-m-d");
        $jam = date("H:i:s");
        // $id_user = Session('id');
        $id_user = $req->id_user;
        $kd_karyawan = $req->kd_karyawan;
        $dikerjakan = $req->dikerjakan;
        $latlong = $req->latlong;

        $data_karyawan = DB::table('karyawan as a')
                            ->where('a.kd_karyawan', $kd_karyawan)
                            ->leftJoin('posisi_karyawan as c', 'a.id_posisi_karyawan', 'c.id')
                            ->select('a.kd_karyawan', 'a.nama', 'c.posisi')
                            ->first();

        // if (empty($id_user)) {
        //     return redirect()->route('login');
        // }

        $tgl_max_masuk = DB::table('presensi')
                            ->max('tgl_masuk');
        
        $cek_presensi_masuk = DB::table('presensi')
                                ->where('tgl_masuk', $tgl_max_masuk)
                                ->where('kd_karyawan', $data_karyawan->kd_karyawan)
                                ->first();

        $mp = $cek_presensi_masuk->mp;
        
        if (!empty($cek_presensi_masuk)) {
            $absen_pulang = [
                                'tgl_pulang' => $tgl,
                                'jam_pulang' => $jam,
                                'latlong_pulang' => $latlong,
                                'mp' => $mp + 1,
                                'dikerjakan' => $dikerjakan
                            ];
            $log_dikerjakan = [
                                'id_presensi' => $cek_presensi_masuk->id,
                                'dikerjakan' => $dikerjakan,
                            ];
            if ($mp == 3) {
                $notif = (object)[
                            'code' => 300,
                            'message' => 'Sudah mencapai limit pulang 3x !'
                        ];
            }else {
                $update_absen_masuk = DB::table('presensi')
                                    ->where('kd_karyawan', $data_karyawan->kd_karyawan)
                                    ->where('tgl_masuk', $tgl_max_masuk)
                                    ->update($absen_pulang);

                if ($update_absen_masuk) {
                    $insert_log = DB::table('log_dikerjakan')->insert($log_dikerjakan);
                    $notif = (object)[
                                'code' => 200,
                                'message' => 'Berhasil absen pulang'
                            ];
                }
            }            
            
        }else {
            $notif = (object)[
                        'code' => 400,
                        'message' => 'Anda sudah absen pulang !'
                    ];
        }
        $data['data'] = $notif;
        return response()->json($data);
    }

    public function profile(Request $req)
    {
        $kd_karyawan = $req->kd_karyawan;
        $karyawan = DB::table('karyawan as a')
                    ->where('a.kd_karyawan', $kd_karyawan)
                    // ->leftJoin('shift as b', 'a.id_shift', '=', 'b.id')
                    ->leftJoin('posisi_karyawan as c', 'a.id_posisi_karyawan', '=', 'c.id')
                    ->select('a.kd_karyawan', 'a.nama', 'a.alamat', 'a.no_telp', 'c.posisi')
                    ->first();
        $data = [];
        $data['data'] = $karyawan;

        return response()->json($data, 200);
    }

    public function presensi_karyawan(Request $req)
    {
        $kd_karyawan = $req->kd_karyawan;
        $presensi_karyawan = DB::table('presensi as a')
                                ->where('a.kd_karyawan', $kd_karyawan)
                                ->leftJoin('karyawan as b', 'a.kd_karyawan', '=', 'b.kd_karyawan')
                                ->select('a.kd_karyawan', 'b.nama', 'a.tgl_masuk', 'a.tgl_pulang', 'a.jam_masuk', 'a.jam_pulang', 'a.dikerjakan')
                                ->get();
        $data = [];
        $data['data'] = $presensi_karyawan;
        return response()->json($data, 200);
    }
}
