<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;
use Session;
use DB;
use Redirect;
use Validator;

class CekKwitansiController extends Controller
{
    public function __construct()
    {
        date_default_timezone_set("Asia/Jakarta");
    }

    public function index()
    {
        return view('admin.CekKwitansi.index');
    }

    public function get_karyawan($id_user)
    {
        $data = DB::table('karyawan')->where('id_users', $id_user)->first();
        return $data->nama;
    }

    public function datatable()
    {
        $parent_jurnal = DB::table('parent_jurnal')
                                ->where('status', 'tutup')
                                ->orderBy('created_at', 'DESC')
                                ->first();

        $tgl_akhir = isset($parent_jurnal) ? $parent_jurnal->tgl_akhir : '';

        $kwitansi = DB::table('kwitansi')
                            ->whereDate('tgl', '<', $tgl_akhir)
                            ->get();

        $no_kwi = [];

        foreach ($kwitansi as $value) {
            $no_kwi[] = $value->no_kwi;
        }

        $data = DB::table('kwitansi')
                            ->whereNotIn('no_kwi', $no_kwi)
                            ->get();

        return Datatables::of($data)
        ->editColumn('is_cek_kwi', function ($data) {
            $is_cek_kwi = $data->is_cek_kwi;
            $nama = isset($is_cek_kwi) ? $this->get_karyawan($data->cek_kwi) : '-';
            
            if ($is_cek_kwi == 1) {
                return '<center><i class="fa fa-check"></i>'.$nama.'</center>';
            } else if ($is_cek_kwi == 2) {
                return '<center><i class="fa fa-close"></i>'.$nama.'</center>';
            } else if ($is_cek_kwi == '') {
                return '<center><i class="fa fa-minus"></center>';
            } else {
            
            }
        }) 
        ->addColumn('opsi', function ($data){
            $no_kwi = $data->no_kwi;
            $tgl = date("d-m-Y", strtotime($data->tgl));
            $dari = $data->dari;
            $penerima = $data->penerima;
            $total = $data->total;
            $cek_kwi = ($data->is_cek_kwi == 1) ? 'acc' : 'tidak acc';

            return '<button type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#modal_cekkwi" data-form="cek_kwi" data-status="'.$cek_kwi.'" data-id="'.$no_kwi.'" data-tgl="'.$tgl.'" data-dari="'.$dari.'" data-penerima="'.$penerima.'" data-total="'.$total.'">Cek Kwitansi</button>';
        })
        ->rawColumns(['is_cek_kwi', 'opsi'])
        ->make(true);
    }

    public function datatable_beli(Request $req)
    {
        $id = $req->_id;
        $data = DB::table('beli as a')
                    ->where('a.status', NULL)
                    ->where('a.id_beli', $id)
                    ->leftJoin('beli_detail as b', 'a.id_beli', '=', 'b.id_detail_beli')
                    ->leftJoin('satuan as c', 'b.id_satuan', '=', 'c.id')
                    ->select('a.id','a.id_beli','b.nama_brg','b.ketr','b.qty','b.harga','b.subtotal',
                                    'c.nama as satuan')
                    ->get();

        return Datatables::of($data)  
        ->addIndexColumn() 
        ->make(true);                                        
    }

    public function acc_kwitansi(Request $req)
    {
        $id_users = session::get('id_user');
        $no_kwi = $req->_noKwi;
        $btn = $req->_btn;
        $status = $req->_status;
        
        $data_acc_kwi = [
            'is_cek_kwi' => 1,
            'cek_kwi' => $id_users,
            'user_upd' => $id_users,
            'updated_at' => date("Y-m-d H:i:s")
        ];

        $data_batal_kwi = [
            'is_cek_kwi' => 2,
            'cek_kwi' => $id_users,
            'user_upd' => $id_users,
            'updated_at' => date("Y-m-d H:i:s")
        ];
        
        if($status != 'acc') {
            if ($btn == 'setuju') {
                $update_kwitansi = DB::table('kwitansi')
                                ->where('no_kwi', $no_kwi)
                                ->update($data_acc_kwi);
                                
                if ($update_kwitansi) {
                    $res = [
                        'code' => 201,
                        'msg' => 'Kwitansi Berhasil Di ACC'
                    ];
                } else {
                    $res = [
                        'code' => 400,
                        'msg' => 'Kwitansi gagal di ACC'
                    ];
                }
            } else if ($btn == 'batal') {
                $update_kwitansi = DB::table('kwitansi')
                            ->where('no_kwitansi', $id)
                            ->update($data_batal_kwitansi);
                            
                if ($update_kwitansi) {
                    $res = [
                        'code' => 300,
                        'msg' => 'Kwitansi Batal di Acc'
                    ];
                } else {
                    $res = [
                        'code' => 400,
                        'msg' => 'Kwitansi gagal untuk dibatalkan'
                    ];
                }
            }
        } else {
            $res = [
                'code' => 400,
                'msg' => 'Kwitansi sudah di ACC'
            ];
        }
        return response()->json($res);
    }
}
