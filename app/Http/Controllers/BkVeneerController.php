<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;
use Session;
use DB;
use Redirect;
use Validator;
use Exception;

class BkVeneerController extends Controller
{
    public function __contruct()
    {
        date_default_timezone_set("Asia/Jakarta");
    }

    public function index()
    {
        return view('admin.bkVeneer.index');
    }

    public function datatable()
    {
        $data = DB::table('bk as a')
                        ->leftJoin('bk_kepada as b', 'a.id_kepada', '=', 'b.id')
                        ->where('a.status', NULL)
                        ->where('form', 'veneer')
                        ->orderBy('tgl', 'DESC')
                        ->orderBy('no_bk', 'DESC')
                        ->get();

        return Datatables::of($data)
        ->addIndexColumn()
        ->editColumn('no_bk', function ($data) {
            return 'bk '.$data->no_bk;
        })
        ->editColumn('tgl', function ($data) {
            return date('d-m-Y', strtotime($data->tgl));
        })
        ->editColumn('total', function ($data) {
            return number_format($data->total, 0, ',', '.');
        })
        ->addColumn('opsi', function ($data) {
            $no_bk = $data->no_bk;
            $edit = route('bkVeneer.form_edit', [base64_encode($no_bk)]);
            $status_btn = isset($data->is_cek_bk) ? 'disabled' : '';

            return '<a href="'.$edit.'" class="btn btn-sm btn-primary" '.$status_btn.'><i class="fa fa-edit"></i><a/>
            <button type="button" class="btn btn-sm btn-danger" '.$status_btn.' onclick="delete_bk('.$no_bk.')"><i class="fa fa-trash"></i></button>';
        })
        ->rawColumns(['opsi'])
        ->make(true);
    }

    public function no_bk()
    {
        $no_bk = DB::table('bk')
                        ->where('status',NULL)
                        ->max('no_bk');

        $no = isset($no_bk) ? $no_bk : 0;
        $no++;

        return response()->json($no);
    }

    public function form()
    {
        $kepada = DB::table('bk_kepada')
                        ->where('status', NULL)
                        ->get();

        $satuan = DB::table('satuan')
                        ->where('status', NULL)
                        ->get();

        $data['kepada'] = $kepada;
        $data['satuan'] = $satuan;
        return view('admin.bkVeneer.form')->with($data);
    }

    public function get_nama_veneer(Request $req)
    {
        $id = $req->_id;
        $data = DB::table('veneer')->where('id', $id)->first();
        $nama = $data->p.' x '.$data->l.' x '.$data->t.' '.$data->jenis.' kw-'.$data->kw;
        return response()->json($nama);
    }

    public function datatable_brg()
    {
        $data = DB::table('veneer as a')
                        ->leftJoin('barang as b', 'a.id_veneer', '=', 'b.kode')
                        ->leftJoin('jenis_barang as c', 'b.jenis_brg', '=', 'c.id')
                        ->select('a.id','a.id_veneer','a.p', 'a.l', 'a.t', 'a.jenis', 'a.kw', 'a.veneer', 
                                    'b.harga','b.hpp','b.hrg_veneer_basah',
                                    'c.jenis_brg')
                        ->get();
        
        return Datatables::of($data)
        ->addIndexColumn()
        ->addColumn('nama_brg', function ($data) {
            $nama_brg = $data->p.' x '.$data->l.' x '.$data->t.' '.$data->jenis.' kw-'.$data->kw;
            return $nama_brg;
        })
        ->addColumn('opsi', function ($data){
            $id = $data->id;
            $id_veneer = $data->id_veneer;
            $panjang = isset($data->p) ? $data->p : 0;
            $lebar = isset($data->l) ? $data->l : 0;
            $tebal = isset($data->t) ? $data->t : 0;
            $jenis_veneer = "'".$data->jenis_brg."'";
            $harga = 0;
            if ($data->jenis_brg == 'Veneer Basah') {
                $harga = $data->hrg_veneer_basah;
            } elseif ($data->jenis_brg == 'Veneer Kering') {
                $harga = $data->hpp;
            } else {
                $harga = $data->harga;
            }
            return '<button id="btn_pilih" type="button" class="btn btn-sm btn-primary" onclick="select_brg('.$id.','.$id_veneer.','.$harga.','.$panjang.','.$lebar.','.$tebal.','.$jenis_veneer.')">Pilih</button>';
        })
        ->rawColumns(['opsi', 'nama_brg'])
        ->make(true);
    }

    public function get_total($no_bk, $grup)
    {
        $total = DB::table('jurnal')
                            ->where('bk',$no_bk)
                            ->where('jenis_jurnal', 'bk')
                            ->where('grup', $grup)
                            ->where('status', NULL)
                            ->sum('total');

        return $total;
    }

    public function set_akun($tipe, $data_bk, $data_bk_detail, $insert_bk_detail)
    {
        $bk_kepada = DB::table('bk_kepada')->where('id', $data_bk['id_kepada'])->first();
        $kepada = isset($bk_kepada) ? $bk_kepada->kepada : '';
        $keterangan = isset($data_bk_detail['keterangan']) ? ' // '.$data_bk_detail['keterangan'] : '';

        $barang = DB::table('barang')->where('kode', $data_bk_detail['id_brg'])->first();

        $harga_veneer = 0;

        if ($data_bk_detail['jenis_veneer'] == 'Veneer Basah') {
            $harga_veneer = $barang->hrg_veneer_basah;
        } elseif ($data_bk_detail['jenis_veneer'] == 'Veneer Kering') {
            $harga_veneer = $barang->hrg_veneer_basah;
        } elseif ($data_bk_detail['jenis_veneer'] == 'Veneer Jadi') {
            $harga_veneer = $barang->hpp;
        }

        if ($tipe == 'insert') {
            $akun[0]['tgl'] = $data_bk['tgl'];
            $akun[0]['id_item'] = null;
            $akun[0]['no_akun'] = '411';
            $akun[0]['jenis_jurnal'] = 'bk';
            $akun[0]['bk'] = strtolower($data_bk['no_bk']);
            $akun[0]['nama'] = $kepada;
            $akun[0]['keterangan'] = 'veneer hasil tembel';
            $akun[0]['map'] = 'd';
            $akun[0]['hit'] = null;
            $akun[0]['grup'] = 1;
            $akun[0]['qty'] = null;
            $akun[0]['m3'] = null;
            $akun[0]['harga'] = null;
            $akun[0]['total'] = null;

            $akun[1]['tgl'] = $data_bk['tgl'];
            $akun[1]['id_item'] = $insert_bk_detail;
            $akun[1]['no_akun'] = '250';
            $akun[1]['jenis_jurnal'] = 'bk';
            $akun[1]['bk'] = strtolower($data_bk['no_bk']);
            $akun[1]['nama'] = $kepada;
            $akun[1]['keterangan'] = $data_bk_detail['nama_brg'].$keterangan;
            $akun[1]['map'] = 'd';
            $akun[1]['hit'] = 'm';
            $akun[1]['grup'] = 2;
            $akun[1]['qty'] = $data_bk_detail['qty'];
            $akun[1]['m3'] = $data_bk_detail['m3'];
            $akun[1]['harga'] = $harga_veneer;
            $akun[1]['total'] = $data_bk_detail['m3'] * $harga_veneer;

            $akun[2]['tgl'] = $data_bk['tgl'];
            $akun[2]['id_item'] = $insert_bk_detail;
            $akun[2]['no_akun'] = '141';
            $akun[2]['jenis_jurnal'] = 'bk';
            $akun[2]['bk'] = strtolower($data_bk['no_bk']);
            $akun[2]['nama'] = $kepada;
            $akun[2]['keterangan'] = $data_bk_detail['nama_brg'].$keterangan;
            $akun[2]['map'] = 'k';
            $akun[2]['hit'] = 'm';
            $akun[2]['grup'] = 3;
            $akun[2]['qty'] = $data_bk_detail['qty'];
            $akun[2]['m3'] = $data_bk_detail['m3'];
            $akun[2]['harga'] = $data_bk_detail['harga'];
            $akun[2]['total'] = $data_bk_detail['subtotal'];

            // dd($akun);
            $insert_jurnal = DB::table('jurnal')->insert($akun);
        } elseif ($tipe == 'add_item') {
            $akun[0]['tgl'] = $data_bk['tgl'];
            $akun[0]['id_item'] = $insert_bk_detail;
            $akun[0]['no_akun'] = '250';
            $akun[0]['jenis_jurnal'] = 'bk';
            $akun[0]['bk'] = strtolower($data_bk['no_bk']);
            $akun[0]['nama'] = $kepada;
            $akun[0]['keterangan'] = $data_bk_detail['nama_brg'].$keterangan;
            $akun[0]['map'] = 'd';
            $akun[0]['hit'] = 'm';
            $akun[0]['grup'] = 2;
            $akun[0]['qty'] = $data_bk_detail['qty'];
            $akun[0]['m3'] = $data_bk_detail['m3'];
            $akun[0]['harga'] = $harga_veneer;
            $akun[0]['total'] = $data_bk_detail['m3'] * $harga_veneer;

            $akun[1]['tgl'] = $data_bk['tgl'];
            $akun[1]['id_item'] = $insert_bk_detail;
            $akun[1]['no_akun'] = '141';
            $akun[1]['jenis_jurnal'] = 'bk';
            $akun[1]['bk'] = strtolower($data_bk['no_bk']);
            $akun[1]['nama'] = $kepada;
            $akun[1]['keterangan'] = $data_bk_detail['nama_brg'].$keterangan;
            $akun[1]['map'] = 'k';
            $akun[1]['hit'] = 'm';
            $akun[1]['grup'] = 3;
            $akun[1]['qty'] = $data_bk_detail['qty'];
            $akun[1]['m3'] = $data_bk_detail['m3'];
            $akun[1]['harga'] = $data_bk_detail['harga'];
            $akun[1]['total'] = $data_bk_detail['subtotal'];

            $insert_jurnal = DB::table('jurnal')->insert($akun);
        }

        $total_pendapatan_debit =  $this->get_total($data_bk['no_bk'], 2);
        $total_pendapatan_kredit =  $this->get_total($data_bk['no_bk'], 3);
        $total_pendapatan = $total_pendapatan_kredit - $total_pendapatan_debit;

        $upd_jurnal = DB::table('jurnal')
                            ->where('bk', $data_bk['no_bk'])
                            ->where('jenis_jurnal', 'bk')
                            ->where('no_akun', '411')
                            ->where('map', 'd')
                            ->update([
                                'harga'     => $total_pendapatan,
                                'total'     => $total_pendapatan
                            ]);
    }

    public function update_jurnal($data_bk)
    {
        $bk_kepada = DB::table('bk_kepada')->where('id', $data_bk['id_kepada'])->first();
        $kepada = isset($bk_kepada) ? $bk_kepada->kepada : '';
        $update_jurnal = DB::table('jurnal')
                                ->where('jenis_jurnal', 'bk')
                                ->where('bk', $data_bk['no_bk'])
                                ->update([
                                    'tgl'    => $data_bk['tgl'],
                                    'nama'   => $kepada,
                                ]);
    }

    public function save(Request $req)
    {
        $id_user = session::get('id_user');
        $no_bk = $req->_noBk;
        $id_kepada = $req->_idKepada;
        $bk_kepada = DB::table('bk_kepada')
                            ->where('status', NULL)
                            ->where('id', $id_kepada)
                            ->first();
        $kepada = isset($bk_kepada) ? $bk_kepada->kepada : '';
        $tgl = $req->_tgl;
        $ketr = $req->_ketr; 
        
        $id_item = $req->_idItem;
        $id_brg = $req->_idBrg; 
        $nama_brg = $req->_namaBrg; 
        $jenis_veneer = $req->_jenisVeneer;
        $id_satuan = $req->_idSatuan; 
        $qty = $req->_qty; 
        $harga = $req->_harga;
        $m3 = $req->_m3;
        $subtotal = $req->_subtotal;
        $keterangan = $req->_keterangan;

        $data_bk = [
            'no_bk'         => $no_bk,
            'form'          => 'veneer',
            'tgl'           => date('Y-m-d', strtotime($tgl)),
            'id_kepada'     => $id_kepada,
            'kepada'        => $kepada,
            'ket'           => $ketr,
            'created_at'    => date('Y-m-d H:i:s'),
            'user_add'      => $id_user
        ];

        $data_bk_detail = [
            'no_bk'         => $no_bk,
            'id_brg'        => $id_brg, 
            'nama_brg'      => $nama_brg, 
            'jenis_veneer'  => $jenis_veneer,
            'id_satuan'     => $id_satuan, 
            'qty'           => $qty,
            'harga'         => $harga,
            'm3'            => $m3,
            'subtotal'      => $subtotal,
            'keterangan'    => $keterangan, 
        ];

        $cek_bk = DB::table('bk')->where('no_bk', $no_bk)->where('status', NULL)->first();
        $res = [];
        try {
            if (!$tgl || !$id_kepada) {
                $res = [
                    'code' => 400,
                    'msg' => 'Data Belum Lengkap'
                ];
            } else {
                if(is_null($cek_bk)) {
                    $insert_bk = DB::table('bk')->insert($data_bk);
                    $insert_bk_detail = DB::table('bk_detail')->insertGetId($data_bk_detail);
                    $total = DB::table('bk_detail')->where('no_bk', $no_bk)->where('status', NULL)->sum('subtotal');
                    $update_total_bk = DB::table('bk')->where('no_bk', $no_bk)->update([
                        'total' => $total
                    ]);

                    $this->set_akun('insert', $data_bk, $data_bk_detail, $insert_bk_detail);

                    $res = [
                        'code' => 300,
                        'msg' => 'Data Berhasil disimpan'
                    ];
                } else if (!is_null($cek_bk)) {
                    if (isset($id_item)) {
                        $bk_detail = DB::table('bk_detail')->where('id', $id_item)->first();
                        $update_item = DB::table('bk_detail')->where('id', $id_item)->update($data_bk_detail);
                        $total = DB::table('bk_detail')->where('no_bk', $no_bk)->where('status', NULL)->sum('subtotal');
                        $update_total_bk = DB::table('bk')->where('no_bk', $no_bk)->update([
                            'total' => $total
                        ]);

                        $res = [
                            'code' => 200,
                            'msg' => 'Data Barang Berhasil Diupdate',
                        ];
                    } else {
                        if (isset($nama_brg)) {
                            $insert_bk_detail = DB::table('bk_detail')->insertGetId($data_bk_detail);
                            $total = DB::table('bk_detail')->where('no_bk', $no_bk)->where('status', NULL)->sum('subtotal');
                            $update_total_bk = DB::table('bk')->where('no_bk', $no_bk)->update([
                                'total' => $total
                            ]);

                            $this->set_akun('add_item', $data_bk, $data_bk_detail, $insert_bk_detail);

                            $res = [
                                'code' => 200,
                                'msg' => 'Data Barang Berhasil Disimpan',
                            ];
                        }
                        $update_bk =  DB::table('bk')->where('no_bk', $no_bk)->update($data_bk);
                        $this->update_jurnal($data_bk);
                        $res = [
                            'code' => 201,
                            'msg' => 'BK Berhasil Diupdate',
                        ];
                    }
                }
            }
        } catch (Exception $th) {
            $res = [
                'code' => 400,
                'msg' => 'Data Gagal disimpan'
            ];
        }
        return response()->json($res);
    }

    public function datatable_detail(Request $req)
    {
        $no_bk = $req->_noBk;

        $data = DB::table('bk_detail as a')
                        ->leftJoin('satuan as b', 'a.id_satuan', '=', 'b.id')
                        ->where('a.no_bk', $no_bk)
                        ->where('a.status', NULL)
                        ->select('a.id', 'a.no_bk', 'a.id_brg', 'a.nama_brg', 'a.akun_brg', 'a.keterangan', 'a.qty', 'a.m3', 'a.harga', 'a.subtotal',
                                    'b.nama as satuan')
                        ->get();

        return Datatables::of($data)
        ->addIndexColumn()
        ->addColumn('opsi', function ($data) {
            return '<button class="btn btn-sm btn-danger" onclick="delete_item('.$data->id.')"><i class="fa fa-trash"></i></button>';
        })
        ->editColumn('harga', function ($data) {
            return number_format($data->harga, 0, ',', '.');
        })
        ->editColumn('subtotal', function ($data) {
            return number_format($data->subtotal);
        })
        ->rawColumns(['opsi'])
        ->make(true);
    }

    public function delete_item(Request $req)
    {
        $id_item = $req->_idItem;

        $bk = DB::table('bk_detail')->where('id', $id_item)->where('status', NULL)->select('no_bk')->first();

        try {
            $delete = DB::table('bk_detail')->where('id', $id_item)->where('status', NULL)->delete();
            $total = DB::table('bk_detail')->where('no_bk',$bk->no_bk)->where('status',NULL)->sum('subtotal');
            $update_total = DB::table('bk')->where('no_bk', $bk->no_bk)->update([
                'total' => $total
            ]);

            $delete_item_jurnal = DB::table('jurnal')
                                        ->where('jenis_jurnal', 'bk')
                                        ->where('id_item', $id_item)
                                        ->update(['status' => 9]);

            $total_pendapatan_debit =  $this->get_total($bk->no_bk, 2);
            $total_pendapatan_kredit =  $this->get_total($bk->no_bk, 3);
            $total_pendapatan = $total_pendapatan_kredit - $total_pendapatan_debit;
    
            $upd_jurnal = DB::table('jurnal')
                                ->where('bk', $bk->no_bk)
                                ->where('jenis_jurnal', 'bk')
                                ->where('no_akun', '411')
                                ->where('map', 'd')
                                ->update([
                                    'harga'     => $total_pendapatan,
                                    'total'     => $total_pendapatan
                                ]);

            $res = [
                'code' => 300,
                'msg' => 'Data telah dihapus'
            ];
        } catch (Exception $th) {
            $res = [
                'code' => 400,
                'msg' => 'Data Gagal dihapus'
            ];
        }

        $data['response'] = $res;
        return response()->json($data);
    }

    public function form_edit($no_bk)
    {
        $nobk = base64_decode($no_bk);
        $bk = DB::table('bk')
                        ->where('no_bk', $nobk)
                        ->where('status', NULL)
                        ->first();

        $kepada = DB::table('bk_kepada')
                        ->where('status', NULL)
                        ->get();

        $satuan = DB::table('satuan')
                        ->where('status', NULL)
                        ->get();

        $data['no_bk'] = $nobk;
        $data['tgl'] = date('d-m-Y', strtotime($bk->tgl));
        $data['id_kepada'] = $bk->id_kepada;
        $data['ketr'] = $bk->ket;
        $data['kepada'] = $kepada;
        $data['satuan'] = $satuan;
        $data['form'] = 'edit';

        return view('admin.bkVeneer.form')->with($data);
    }

    public function delete(Request $req)
    {
        $no_bk = $req->_noBk;

        try {
            $delete_bk = DB::table('bk')->where('no_bk', $no_bk)->where('status', NULL)->delete();
            $delete_bk_detail = DB::table('bk_detail')->where('no_bk', $no_bk)->where('status', NULL)->delete();
            $delete_jurnal = DB::table('jurnal')->where('jenis_jurnal', 'bk')->where('bk', $no_bk)->delete();

            $res = [
                'code' => 300,
                'msg' => 'Data telah dihapus'
            ];
        } catch (Exception $th) {
            $res = [
                'code' => 400,
                'msg' => 'Data Gagal dihapus'
            ];
        }

        $data['response'] = $res;
        return response()->json($data);
    }

}
