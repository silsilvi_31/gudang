<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;
use Session;
use DB;
use Redirect;
use Validator;
use File;
use Excel;
use App\Exports\StokExport;

class StokController extends Controller
{
    private $apina;
    private $barangQ;
    private $url;
    private $beli;
    private $jual;
    private $retur_beli;
    private $retur_jual;
    private $jurnal_umum_msk;
    private $jurnal_umum_klr;
    private $opname_;

    private $pembelian;
    private $penjualan;
    private $beli_retur;
    private $jual_retur;
    private $ju_masuk;
    private $ju_keluar;

    public function __construct()
    {
        date_default_timezone_set("Asia/Jakarta"); 
        // $this->url = 'http://adm.wijayaplywoodsindonesia.com/api/ina/stok';
        // $this->url = 'http://192.168.5.9:8080/api/ina/stok';
        // $this->apina = json_decode(file_get_contents($this->url), true);
        $this->barangQ = DB::table('barang')
                                ->where('status', NULL)
                                ->get();

        $this->beli = DB::table('jurnal')
                            ->where('jenis_jurnal', 'beli')
                            // ->where('bm', 1)
                            // ->whereIn('bm', [1,2])
                            ->where('map', 'd')
                            ->groupBy('bm')
                            ->get();
        
        $this->jual = DB::table('jurnal')
                            ->where('jenis_jurnal', 'sj')
                            // ->where('ref', 1)
                            // ->whereIn('ref', [1,2])
                            ->where('map', 'k')
                            ->groupBy('ref')
                            ->get();

        $this->retur_beli = DB::table('jurnal')
                            ->where('jenis_jurnal', 'beli-retur')
                            // ->where('bm', 1)
                            // ->whereIn('bm', [1,2])
                            ->where('map', 'k')
                            ->groupBy('bm')
                            ->get();

        $this->retur_jual = DB::table('jurnal')
                            ->where('jenis_jurnal', 'sj-retur')
                            // ->where('ref', 1)
                            // ->whereIn('ref', [1,2])
                            ->where('map', 'd')
                            ->groupBy('ref')
                            ->get();

        $this->jurnal_umum_msk = DB::table('jurnal')
                            ->where('jenis_jurnal', 'ju')
                            // ->where('ref', 1)
                            // ->whereIn('ref', [1,2])
                            ->where('map', 'd')
                            ->whereIn('no_akun', ['140', '150', '160', '170'])
                            ->groupby('ref')
                            ->get();

        $this->jurnal_umum_klr = DB::table('jurnal')
                            ->where('jenis_jurnal', 'ju') 
                            // ->where('ref', 1)
                            // ->whereIn('ref', [1,2])
                            ->where('map', 'k')
                            ->whereIn('no_akun', ['140', '150', '160', '170'])
                            ->groupBy('ref')
                            ->get();

        $this->pembelian =DB::table('beli_detail as a')
                                ->leftJoin('beli as b', 'a.id_detail_beli', '=', 'b.id_beli')
                                // ->where('a.id_detail_beli', $id)
                                ->whereNotNull('b.is_cek_beli')
                                ->whereNull('b.user_batal')
                                ->get();

        $this->penjualan = DB::table('suratjalan_detail as a')
                                ->leftJoin('suratjalan as b', 'a.id_sj', '=', 'b.id')
                                // ->where('a.id_sj', $id)
                                ->whereNotNull('b.is_cek_nota')
                                ->whereNull('b.is_batal')
                                ->get();

        $this->beli_retur = DB::table('retur_brg')
                                // ->where('id_sj', $id)
                                ->where('jenis', 'beli')
                                ->get();

        $this->jual_retur = DB::table('retur_brg')
                                // ->where('id_sj', $id)
                                ->where('jenis', 'jual')
                                ->get();

        $this->ju_masuk = DB::table('jurnal_umum_detail as a')
                            ->leftJoin('jurnal_umum as b', 'a.id_ju', '=', 'b.id_ju')
                            // ->where('a.id_ju', $id)
                            ->whereNotNull('b.is_cek_jurnal')
                            ->get();

        $this->ju_keluar = DB::table('jurnal_umum_detail as a')
                            ->leftJoin('jurnal_umum as b', 'a.id_ju', '=', 'b.id_ju')
                            // ->where('a.id_ju', $id)
                            ->whereNotNull('b.is_cek_jurnal')
                            ->get();

        $this->opname_ = DB::table('log_opname_stok')
                            // ->where('tgl', $tgl_)
                            // ->where('id_brg', $data_brg->kode)
                            ->orderBy('created_at', 'DESC')
                            ->select('tgl', 'id_brg', 'nama_brg', 'stok_sesudah', DB::Raw('max(created_at) as ct'))
                            ->groupBy('tgl', 'id_brg', 'nama_brg', 'stok_sesudah')
                            ->get();
    }

    public function index()
    {
        return view('admin.stok.index');
    }

    public function get_karyawan($id_user)
    {
        $data = DB::table('karyawan')->where('id_users', $id_user)->first();
        return $data->nama;
    }

    public function detail($id_brg)
    {
        $kode = base64_decode($id_brg);
        $data['kode'] = $kode;

        $barang = DB::table('barang')->where('kode', $kode)->where('status', NULL)->first();
        $data['nama_brg'] = $barang->nama_brg;
        return view('admin.stok.detail_stok')->with($data);
    }

    public function max_tgl($tgl, $nama_brg)
    {
        $max = DB::table('log_opname_stok')
                            ->where('tgl', $tgl)
                            ->where('nama_brg', $nama_brg)
                            ->select('nama_brg', 'stok_sesudah', DB::Raw('max(id)'))
                            ->groupBy('nama_brg', 'stok_sesudah')
                            ->first();

        return isset($max) ? $max->stok_sesudah : null;
    }

    public function closestnumber($cari, $tgl) {
        $akhir = null;
        foreach ($tgl as $c) {
            if ($c < $cari) {
                $akhir = $c;
            } else if ($c == $cari) {
                return $cari;
            } else if ($c > $cari) {
                return $akhir;
            }
        }
        return $akhir;
    }

    public function tampil_barang($id, $method, $data)
    {
        if($method == 'beli') {
            $hasil = array_filter($data->toArray(), function ($val) use ($id) {
                return $val->id_detail_beli == $id;
            });
        } elseif($method == 'jual') {
            $hasil = array_filter($data->toArray(), function ($val) use ($id) {
                return $val->id_sj == $id;
            });
        }  elseif($method == 'beli-retur') {
            $hasil = array_filter($data->toArray(), function ($val) use ($id) {
                return $val->id_sj == $id;
            });
        } elseif($method == 'sj-retur') {
            $hasil = array_filter($data->toArray(), function ($val) use ($id) {
                return $val->id_sj == $id;
            });
        } elseif($method == 'jurnal-masuk') {
            $hasil = array_filter($data->toArray(), function ($val) use ($id) {
                return $val->id_ju == $id;
            });
        } elseif($method == 'jurnal-keluar') {
            $hasil = array_filter($data->toArray(), function ($val) use ($id) {
                return $val->id_ju == $id;
            });
        }

        return $hasil;
    }

    public function get_sisa($data, $id_brg, $opname_time)
    {
        $cari = [
            'id_brg'    => $id_brg,
            'tgl_opname'  => $opname_time
        ];

        $dty = array_filter($data, function ($value) use ($cari) {
            return $value['id_brg'] == $cari['id_brg'] && $value['tgl_time'] >= $cari['tgl_opname'];
        });

        if (is_null($opname_time)) {
            $dty = array_filter($data, function ($value) use ($cari) {
                return $value['id_brg'] == $cari['id_brg'];
            });
        }

        $total_qty = 0;
        foreach ($dty as $val) {
            $total_qty += $val['qty'];
        }

        return $total_qty;
    }

    public function get_sisa_detail($data, $id_brg, $tgl)
    {
        $cari = [
            'id_brg'    => $id_brg,
            'tgl'       => $tgl
        ];

        $dty = array_filter($data, function ($value) use ($cari) {
            return $value['id_brg'] == $cari['id_brg'] && $value['tgl'] == $cari['tgl'];
        });

        $total_qty = 0;
        foreach ($dty as $val) {
            $total_qty += $val['qty'];
        }

        return $total_qty;
    }

    public function datatable()
    {
        // dd('a');
        $dt_beli = [];
        $dt_jual = [];
        $dt_beli_retur = [];
        $dt_jual_retur = [];
        $dt_msk_jurnal_umum = [];
        $dt_klr_jurnal_umum = [];

        $stok_brg_beli = [];
        $stok_brg_jual = [];
        $stok_brg_beli_retur = [];
        $stok_brg_jual_retur = [];
        $stok_brg_msk_ju = [];
        $stok_brg_klr_ju = [];

        $stokQ = [];

        $pembelian = $this->pembelian;
        $penjualan = $this->penjualan;
        $beli_retur = $this->beli_retur;
        $jual_retur = $this->jual_retur;
        $ju_masuk = $this->ju_masuk;
        $ju_keluar = $this->ju_keluar;
        
        // AWAL JURNAL
        foreach ($this->beli as $value) {
            $dt_beli[] = (object) [
                "tgl"            => $value->tgl,
                "no_akun"        => $value->no_akun,
                "hpp"            => $value->hpp,
                "jenis_jurnal"   => $value->jenis_jurnal,
                "ref"            => $value->ref,
                "bm"             => $value->bm,
                "map"            => $value->map,
                "qty"            => $value->qty,
                "harga"          => $value->harga,
                "total"          => $value->total,
                "barang"         => $this->tampil_barang($value->bm, 'beli', $pembelian)
            ];
        }

        foreach ($this->jual as $value) {
            $dt_jual[] = (object) [
                "tgl"            => $value->tgl,
                "no_akun"        => $value->no_akun,
                "hpp"            => $value->hpp,
                "jenis_jurnal"   => $value->jenis_jurnal,
                "ref"            => $value->ref,
                "bm"             => $value->bm,
                "map"            => $value->map,
                "qty"            => $value->qty,
                "harga"          => $value->harga,
                "total"          => $value->total,
                "barang"         => $this->tampil_barang($value->ref, 'jual', $penjualan)
            ];
        }

        foreach ($this->retur_beli as $value) {
            $dt_beli_retur[] = (object) [
                "tgl"            => $value->tgl,
                "no_akun"        => $value->no_akun,
                "hpp"            => $value->hpp,
                "jenis_jurnal"   => $value->jenis_jurnal,
                "ref"            => $value->ref,
                "bm"             => $value->bm,
                "map"            => $value->map,
                "qty"            => $value->qty,
                "harga"          => $value->harga,
                "total"          => $value->total,
                "barang"         => $this->tampil_barang($value->bm, 'beli-retur', $beli_retur)
            ];
        }

        foreach ($this->retur_jual as $value) {
            $dt_jual_retur[] = (object) [
                "tgl"            => $value->tgl,
                "no_akun"        => $value->no_akun,
                "hpp"            => $value->hpp,
                "jenis_jurnal"   => $value->jenis_jurnal,
                "ref"            => $value->ref,
                "bm"             => $value->bm,
                "map"            => $value->map,
                "qty"            => $value->qty,
                "harga"          => $value->harga,
                "total"          => $value->total,
                "barang"         => $this->tampil_barang($value->ref, 'sj-retur', $jual_retur)
            ];
        }

        foreach ($this->jurnal_umum_msk as $value) {
            $dt_msk_jurnal_umum[] = (object) [
                "tgl"            => $value->tgl,
                "no_akun"        => $value->no_akun,
                "hpp"            => $value->hpp,
                "jenis_jurnal"   => $value->jenis_jurnal,
                "ref"            => $value->ref,
                "bm"             => $value->bm,
                "map"            => $value->map,
                "qty"            => $value->qty,
                "harga"          => $value->harga,
                "total"          => $value->total,
                "barang"         => $this->tampil_barang($value->ref, 'jurnal-masuk', $ju_masuk)
            ];
        }

        foreach ($this->jurnal_umum_klr as $value) {
            $dt_klr_jurnal_umum[] = (object) [
                "tgl"            => $value->tgl,
                "no_akun"        => $value->no_akun,
                "hpp"            => $value->hpp,
                "jenis_jurnal"   => $value->jenis_jurnal,
                "ref"            => $value->ref,
                "bm"             => $value->bm,
                "map"            => $value->map,
                "qty"            => $value->qty,
                "harga"          => $value->harga,
                "total"          => $value->total,
                "barang"         => $this->tampil_barang($value->ref, 'jurnal-keluar', $ju_keluar)
            ];
        }

        // AKHIR JURNAL
        foreach ($dt_beli as $v) {
            foreach ($v->barang as $x) {
               $stok_brg_beli[] = [
                'tgl'           => $v->tgl,
                'tgl_time'      => strtotime($v->tgl),
                'id_brg'        => $x->id_brg,
                'nama_brg'      => $x->nama_brg,
                'qty'           => $x->qty,
                'harga'         => $x->harga
               ];
            }
        }

        foreach ($dt_jual as $v) {
            foreach ($v->barang as $x) {
               $stok_brg_jual[] = [
                'tgl'           => $v->tgl,
                'tgl_time'      => strtotime($v->tgl),
                'id_brg'        => $x->id_brg,
                'nama_brg'      => $x->nama_brg,
                'qty'           => $x->qty,
                'harga'         => $x->harga
               ];
            }
        }

        foreach ($dt_beli_retur as $v) {
            foreach ($v->barang as $x) {
                $stok_brg_beli_retur[] = [
                    'tgl'           => $v->tgl,
                    'tgl_time'      => strtotime($v->tgl),
                    'id_brg'        => $x->id_brg,
                    'nama_brg'      => $x->nama_brg,
                    'qty'           => $x->qty_retur,
                    'harga'         => $x->hpp
                ];
            }
        }

        foreach ($dt_jual_retur as $v) {
            foreach ($v->barang as $x) {
                $stok_brg_jual_retur[] = [
                    'tgl'           => $v->tgl,
                    'tgl_time'      => strtotime($v->tgl),
                    'id_brg'        => $x->id_brg,
                    'nama_brg'      => $x->nama_brg,
                    'qty'           => $x->qty_retur,
                    'harga'         => $x->hpp
                ];
            }
        }

        foreach ($dt_msk_jurnal_umum as $v) {
            foreach ($v->barang as $x) {
                $stok_brg_msk_ju[] = [
                    'tgl'           => $v->tgl,
                    'tgl_time'      => strtotime($v->tgl),
                    'id_brg'        => $x->id_brg,
                    'nama_brg'      => $x->nama_brg,
                    'qty'           => $x->qty,
                    'harga'         => $x->harga
                ];
            }
        }

        foreach ($dt_klr_jurnal_umum as $v) {
            foreach ($v->barang as $x) {
                $stok_brg_klr_ju[] = [
                    'tgl'           => $v->tgl,
                    'tgl_time'      => strtotime($v->tgl),
                    'id_brg'        => $x->id_brg,
                    'nama_brg'      => $x->nama_brg,
                    'qty'           => $x->qty,
                    'harga'         => $x->harga
                ];
            }
        }

        $sisa = 0;
        $stok_awal = 0;
        foreach ($this->barangQ as $b) {
            $opname_time = isset($b->tgl_opname) ? (string)strtotime($b->tgl_opname) : null;
            $stok_awal = isset($b->stok) ? $b->stok : null;

            $stok_beli = $this->get_sisa($stok_brg_beli, $b->kode, $opname_time);
            $stok_jual = $this->get_sisa($stok_brg_jual, $b->kode, $opname_time);
            $stok_beli_retur = $this->get_sisa($stok_brg_beli_retur, $b->kode, $opname_time);
            $stok_jual_retur = $this->get_sisa($stok_brg_jual_retur, $b->kode, $opname_time);
            $stok_msk_jurnal = $this->get_sisa($stok_brg_msk_ju, $b->kode, $opname_time);
            $stok_klr_jurnal = $this->get_sisa($stok_brg_klr_ju, $b->kode, $opname_time);

            $stok_masuk = $stok_beli + $stok_jual_retur + $stok_msk_jurnal;
            $stok_keluar = $stok_jual + $stok_beli_retur + $stok_klr_jurnal;
            $sisa = $stok_awal + $stok_masuk - $stok_keluar;

            $stokQ[] = (object) [
                'id_brg'          => $b->kode,
                'nama_brg'        => $b->nama_brg,
                'stok_beli'       => $stok_beli,
                'stok_jual'       => $stok_jual,
                'stok_beli_retur' => $stok_beli_retur,
                'stok_jual_retur' => $stok_jual_retur,
                'stok_msk_jurnal' => $stok_msk_jurnal,
                'stok_klr_jurnal' => $stok_klr_jurnal,
                'stok_awal'       => $stok_awal,
                'stok_masuk'      => $stok_masuk,
                'stok_keluar'     => $stok_keluar,
                'sisa'            => $sisa
            ];
        }
        // dd($stokQ);
        return datatables::of($stokQ)
        ->addIndexColumn()
        ->addColumn('stok_awal', function ($stokQ){
            return $stokQ->stok_awal;
        })
        ->addColumn('stok_masuk', function ($stokQ){
            return $stokQ->stok_masuk;
        })
        ->addColumn('stok_keluar', function ($stokQ){
            return $stokQ->stok_keluar;
        })
        ->addColumn('opsi', function ($stokQ){
            $link_detail = $sj = route('stok.detail', [base64_encode($stokQ->id_brg)]);
            return '<a href="'.$link_detail.'" class="btn btn-sm btn-primary">Detail</a>';
        })
        ->rawColumns(['stok_awal','stok_masuk', 'stok_keluar', 'sisa', 'opsi'])
        ->make(true);
    }

    public function datatable_detail(Request $req)
    {
        $id_brg = $req->_kodeBrg;
        $data_brg = DB::table('barang')->where('kode', $id_brg)->where('status', NULL)->first();
        
        $opname = DB::table('log_opname_stok')
                        // ->where('tgl', $tgl_)
                        ->where('id_brg', $data_brg->kode)
                        ->orderBy('created_at', 'DESC')
                        ->select('tgl', 'id_brg', 'nama_brg', 'stok_sesudah', DB::Raw('max(created_at) as ct'))
                        ->groupBy('tgl', 'id_brg', 'nama_brg', 'stok_sesudah')
                        ->get();

        $dt_beli = [];
        $dt_jual = [];
        $dt_beli_retur = [];
        $dt_jual_retur = [];
        $dt_msk_jurnal_umum = [];
        $dt_klr_jurnal_umum = [];
        $dt_opname = [];

        $tmp_beli = [];
        $tmp_jual = [];
        $tmp_beli_retur = [];
        $tmp_jual_retur = [];
        $tmp_msk_ju = [];
        $tmp_klr_ju = [];
        $tmp_opname = [];

        $stokQ = [];

        $pembelian = $this->pembelian;
        $penjualan = $this->penjualan;
        $beli_retur = $this->beli_retur;
        $jual_retur = $this->jual_retur;
        $ju_masuk = $this->ju_masuk;
        $ju_keluar = $this->ju_keluar;

        $tmp_tgl_fix = [];

        // AWAL JURNAL
        foreach ($this->beli as $value) {
            $dt_beli[] = (object) [
                "tgl"            => $value->tgl,
                "no_akun"        => $value->no_akun,
                "hpp"            => $value->hpp,
                "jenis_jurnal"   => $value->jenis_jurnal,
                "ref"            => $value->ref,
                "bm"             => $value->bm,
                "map"            => $value->map,
                "qty"            => $value->qty,
                "harga"          => $value->harga,
                "total"          => $value->total,
                "barang"         => $this->tampil_barang($value->bm, 'beli', $pembelian)
            ];
        }

        foreach ($this->jual as $value) {
            $dt_jual[] = (object) [
                "tgl"            => $value->tgl,
                "no_akun"        => $value->no_akun,
                "hpp"            => $value->hpp,
                "jenis_jurnal"   => $value->jenis_jurnal,
                "ref"            => $value->ref,
                "bm"             => $value->bm,
                "map"            => $value->map,
                "qty"            => $value->qty,
                "harga"          => $value->harga,
                "total"          => $value->total,
                "barang"         => $this->tampil_barang($value->ref, 'jual', $penjualan)
            ];
        }

        foreach ($this->retur_beli as $value) {
            $dt_beli_retur[] = (object) [
                "tgl"            => $value->tgl,
                "no_akun"        => $value->no_akun,
                "hpp"            => $value->hpp,
                "jenis_jurnal"   => $value->jenis_jurnal,
                "ref"            => $value->ref,
                "bm"             => $value->bm,
                "map"            => $value->map,
                "qty"            => $value->qty,
                "harga"          => $value->harga,
                "total"          => $value->total,
                "barang"         => $this->tampil_barang($value->bm, 'beli-retur', $beli_retur)
            ];
        }

        foreach ($this->retur_jual as $value) {
            $dt_jual_retur[] = (object) [
                "tgl"            => $value->tgl,
                "no_akun"        => $value->no_akun,
                "hpp"            => $value->hpp,
                "jenis_jurnal"   => $value->jenis_jurnal,
                "ref"            => $value->ref,
                "bm"             => $value->bm,
                "map"            => $value->map,
                "qty"            => $value->qty,
                "harga"          => $value->harga,
                "total"          => $value->total,
                "barang"         => $this->tampil_barang($value->ref, 'sj-retur', $jual_retur)
            ];
        }
        foreach ($this->jurnal_umum_msk as $value) {
            $dt_msk_jurnal_umum[] = (object) [
                "tgl"            => $value->tgl,
                "no_akun"        => $value->no_akun,
                "hpp"            => $value->hpp,
                "jenis_jurnal"   => $value->jenis_jurnal,
                "ref"            => $value->ref,
                "bm"             => $value->bm,
                "map"            => $value->map,
                "qty"            => $value->qty,
                "harga"          => $value->harga,
                "total"          => $value->total,
                "barang"         => $this->tampil_barang($value->ref, 'jurnal-masuk', $ju_masuk)
            ];
        }

        foreach ($this->jurnal_umum_klr as $value) {
            $dt_klr_jurnal_umum[] = (object) [
                "tgl"            => $value->tgl,
                "no_akun"        => $value->no_akun,
                "hpp"            => $value->hpp,
                "jenis_jurnal"   => $value->jenis_jurnal,
                "ref"            => $value->ref,
                "bm"             => $value->bm,
                "map"            => $value->map,
                "qty"            => $value->qty,
                "harga"          => $value->harga,
                "total"          => $value->total,
                "barang"         => $this->tampil_barang($value->ref, 'jurnal-keluar', $ju_keluar)
            ];
        }
        // AKHIR JURNAL

        foreach ($dt_beli as $v) {
            foreach ($v->barang as $x) {
               $tmp_beli[] = [
                'tgl'           => $v->tgl,
                'tgl_time'      => strtotime($v->tgl),
                'id_brg'        => $x->id_brg,
                'nama_brg'      => $x->nama_brg,
                'qty'           => $x->qty,
                'harga'         => $x->harga
               ];
            }
        }

        foreach ($dt_jual as $v) {
            foreach ($v->barang as $x) {
               $tmp_jual[] = [
                'tgl'           => $v->tgl,
                'tgl_time'      => strtotime($v->tgl),
                'id_brg'        => $x->id_brg,
                'nama_brg'      => $x->nama_brg,
                'qty'           => $x->qty,
                'harga'         => $x->harga
               ];
            }
        }

        foreach ($dt_beli_retur as $v) {
            foreach ($v->barang as $x) {
                $tmp_beli_retur[] = [
                    'tgl'           => $v->tgl,
                    'tgl_time'      => strtotime($v->tgl),
                    'id_brg'        => $x->id_brg,
                    'nama_brg'      => $x->nama_brg,
                    'qty'           => $x->qty_retur,
                    'harga'         => $x->hpp
                ];
            }
        }

        foreach ($dt_jual_retur as $v) {
            foreach ($v->barang as $x) {
                $tmp_jual_retur[] = [
                    'tgl'           => $v->tgl,
                    'tgl_time'      => strtotime($v->tgl),
                    'id_brg'        => $x->id_brg,
                    'nama_brg'      => $x->nama_brg,
                    'qty'           => $x->qty_retur,
                    'harga'         => $x->hpp
                ];
            }
        }

        foreach ($dt_msk_jurnal_umum as $v) {
            foreach ($v->barang as $x) {
                $tmp_msk_ju[] = [
                    'tgl'           => $v->tgl,
                    'tgl_time'      => strtotime($v->tgl),
                    'id_brg'        => $x->id_brg,
                    'nama_brg'      => $x->nama_brg,
                    'qty'           => $x->qty,
                    'harga'         => $x->harga
                ];
            }
        }

        foreach ($dt_klr_jurnal_umum as $v) {
            foreach ($v->barang as $x) {
                $tmp_klr_ju[] = [
                    'tgl'           => $v->tgl,
                    'tgl_time'      => strtotime($v->tgl),
                    'id_brg'        => $x->id_brg,
                    'nama_brg'      => $x->nama_brg,
                    'qty'           => $x->qty,
                    'harga'         => $x->harga
                ];
            }
        }

        foreach ($opname as $v) {
            $tmp_opname[] = [
                'tgl'           => $v->tgl,
                'id_brg'        => $v->id_brg,
                'nama_brg'      => $v->nama_brg,
                'qty'           => $v->stok_sesudah
            ];
        }

        $tmp_tgl_fix = [];
        $tmp_tgl_fase1 = array_merge($tmp_beli, $tmp_jual, $tmp_beli_retur, $tmp_jual_retur, $tmp_msk_ju, $tmp_klr_ju, $tmp_opname);

        foreach ($tmp_tgl_fase1 as $e) {
            $tmp_tgl_fix[$e['tgl']] = $e['tgl'];
        }
       
        $hasil = 0;
        $awal = 0;
        $ketr = '';
        foreach (array_sort($tmp_tgl_fix) as $v) {
            $stok_beli = $this->get_sisa_detail($tmp_beli, $data_brg->kode, $v);
            $ketr_beli = ($stok_beli > 0) ? $stok_beli.' <span class="badge bg-blue">beli</span>' : null;
            
            $stok_jual = $this->get_sisa_detail($tmp_jual, $data_brg->kode, $v);
            $ketr_jual = ($stok_jual > 0) ? $stok_jual.' <span class="badge bg-green">jual</span>' : null;

            $stok_beli_retur = $this->get_sisa_detail($tmp_beli_retur, $data_brg->kode, $v);
            $ketr_beli_retur = ($stok_beli_retur > 0) ? $stok_beli_retur.' <span class="badge bg-green">beli retur</span>' : null;

            $stok_jual_retur = $this->get_sisa_detail($tmp_jual_retur, $data_brg->kode, $v);
            $ketr_jual_retur = ($stok_jual_retur > 0) ? $stok_jual_retur.' <span class="badge bg-blue">jual retur</span>' : null;

            $stok_msk_ju = $this->get_sisa_detail($tmp_msk_ju, $data_brg->kode, $v);
            $ketr_msk_ju = ($stok_msk_ju > 0) ? $stok_msk_ju.' <span class="badge bg-blue">masuk ju</span>' : null;

            $stok_klr_ju = $this->get_sisa_detail($tmp_klr_ju, $data_brg->kode, $v);
            $ketr_klr_ju = ($stok_klr_ju > 0) ? $stok_klr_ju.' <span class="badge bg-green">masuk ju</span>' : null;
            
            // $log_opname = DB::table('log_opname_stok')
            //                     ->where('tgl', $v)
            //                     ->where('id_brg', $data_brg->kode)
            //                     ->orderBy('created_at', 'DESC')
            //                     ->select('tgl', 'id_brg', 'nama_brg', 'stok_sesudah', DB::Raw('max(created_at) as ct'))
            //                     ->groupBy('tgl', 'id_brg' ,'nama_brg', 'stok_sesudah')
            //                     ->first();

            $log_opname = $this->opname_->where('tgl', $v)
                                        ->where('id_brg', $data_brg->kode)
                                        ->first();

            if (isset($log_opname)) {
                $awal = $log_opname->stok_sesudah;
                $ketr = '<span class="badge bg-green">opname</span>';
            } else {
                $ketr = '';
            }

            $stok_masuk = ($stok_beli + $stok_jual_retur + $stok_msk_ju);
            $stok_keluar = ($stok_jual + $stok_beli_retur + $stok_klr_ju);

            $hasil = $awal + $stok_masuk - $stok_keluar;

            $stokQ[] = (object) [
                'tgl'               => $v,
                'id_brg'            => $data_brg->kode,
                'nama_brg'          => $data_brg->nama_brg,
                'stok_beli'         => $stok_beli,
                'stok_jual'         => $stok_jual,
                'stok_beli_retur'   => $stok_beli_retur,
                'stok_jual_retur'   => $stok_jual_retur,
                'stok_msk_ju'       => $stok_msk_ju,
                'stok_klr_ju'       => $stok_klr_ju,
                'stok_awal'         => $awal,
                'stok_masuk'        => $stok_masuk,
                'stok_keluar'       => $stok_keluar,
                'sisa'              => $hasil,
                'ketr'              => $ketr,
                'ketr_b'            => $ketr_beli,
                'ketr_j'            => $ketr_jual,
                'ketr_b_retur'      => $ketr_beli_retur,
                'ketr_j_retur'      => $ketr_jual_retur,
                'ketr_msk_ju'       => $ketr_msk_ju,
                'ketr_klr_ju'       => $ketr_klr_ju
                
            ];
            $awal = $hasil;
        }

        return datatables::of($stokQ)
        ->addIndexColumn()
        ->addColumn('stok_awal', function ($stokQ){
            return $stokQ->stok_awal.' '.$stokQ->ketr;
        })
        ->addColumn('stok_masuk', function ($stokQ){
            // $br = isset($ketr_m) ? '<br>' : null;
            return '<table><tr>
                        <td rowspan="2">'.$stokQ->stok_masuk.'</td>
                        <td>'.$stokQ->ketr_b.'</td>
                    </tr>
                   </table>';
                //    '<table><tr>
                //         <td rowspan="2">'.$stokQ->stok_masuk.'</td>
                //         <td>'.$stokQ->ketr_m.$br.$stokQ->ketr_b.'</td>
                //     </tr>
                //    </table>';
        })
        ->addColumn('stok_keluar', function ($stokQ){
            return '<table><tr>
                        <td rowspan="2">'.$stokQ->stok_keluar.'</td>
                        <td>'.$stokQ->ketr_j.'</td>
                    </tr>
                   </table>';
        })
        ->addColumn('sisa', function ($stokQ){
            return $stokQ->sisa;
        })
        ->addColumn('opsi', function ($stokQ){
            return '<a href="" class="btn btn-sm btn-primary">Detail</a>';
        })
        ->rawColumns(['stok_awal','stok_masuk', 'stok_keluar', 'sisa', 'opsi'])
        ->make(true);
    }

    //tampil_st
    public function tampil_stok_harian($tgl, $id_brg,
                                        $pembelian, $penjualan, $beli_retur, $jual_retur,
                                            $ju_masuk, $ju_keluar)
    {
        // $data_brg = DB::table('barang')->where('kode', $id_brg)->where('status', NULL)->first();

        // $opname = DB::table('log_opname_stok')
        //                 // ->where('tgl', $tgl_)
        //                 ->where('id_brg', $data_brg->kode)
        //                 ->orderBy('created_at', 'DESC')
        //                 ->select('tgl', 'id_brg', 'nama_brg', 'stok_sesudah', DB::Raw('max(created_at) as ct'))
        //                 ->groupBy('tgl', 'id_brg', 'nama_brg', 'stok_sesudah')
        //                 ->get();

        $data_brg = $this->barangQ->where('kode', $id_brg)->first();
        $opname = collect($this->opname_)->where('id_brg', $data_brg->kode);

        $dt_beli = [];
        $dt_jual = [];
        $dt_beli_retur = [];
        $dt_jual_retur = [];
        $dt_msk_jurnal_umum = [];
        $dt_klr_jurnal_umum = [];
        $dt_opname = [];

        $tmp_beli = [];
        $tmp_jual = [];
        $tmp_beli_retur = [];
        $tmp_jual_retur = [];
        $tmp_msk_ju = [];
        $tmp_klr_ju = [];
        $tmp_opname = [];

        $stokQ = [];

        // $pembelian = $this->pembelian;
        // $penjualan = $this->penjualan;
        // $beli_retur = $this->beli_retur;
        // $jual_retur = $this->jual_retur;
        // $ju_masuk = $this->ju_masuk;
        // $ju_keluar = $this->ju_keluar;

        $tmp_tgl_fix = [];

        // AWAL JURNAL
        foreach ($this->beli as $value) {
            $dt_beli[] = (object) [
                "tgl"            => $value->tgl,
                "no_akun"        => $value->no_akun,
                "hpp"            => $value->hpp,
                "jenis_jurnal"   => $value->jenis_jurnal,
                "ref"            => $value->ref,
                "bm"             => $value->bm,
                "map"            => $value->map,
                "qty"            => $value->qty,
                "harga"          => $value->harga,
                "total"          => $value->total,
                "barang"         => $this->tampil_barang($value->bm, 'beli', $pembelian)
            ];
        }

        foreach ($this->jual as $value) {
            $dt_jual[] = (object) [
                "tgl"            => $value->tgl,
                "no_akun"        => $value->no_akun,
                "hpp"            => $value->hpp,
                "jenis_jurnal"   => $value->jenis_jurnal,
                "ref"            => $value->ref,
                "bm"             => $value->bm,
                "map"            => $value->map,
                "qty"            => $value->qty,
                "harga"          => $value->harga,
                "total"          => $value->total,
                "barang"         => $this->tampil_barang($value->ref, 'jual', $penjualan)
            ];
        }

        foreach ($this->retur_beli as $value) {
            $dt_beli_retur[] = (object) [
                "tgl"            => $value->tgl,
                "no_akun"        => $value->no_akun,
                "hpp"            => $value->hpp,
                "jenis_jurnal"   => $value->jenis_jurnal,
                "ref"            => $value->ref,
                "bm"             => $value->bm,
                "map"            => $value->map,
                "qty"            => $value->qty,
                "harga"          => $value->harga,
                "total"          => $value->total,
                "barang"         => $this->tampil_barang($value->bm, 'beli-retur', $beli_retur)
            ];
        }

        foreach ($this->retur_jual as $value) {
            $dt_jual_retur[] = (object) [
                "tgl"            => $value->tgl,
                "no_akun"        => $value->no_akun,
                "hpp"            => $value->hpp,
                "jenis_jurnal"   => $value->jenis_jurnal,
                "ref"            => $value->ref,
                "bm"             => $value->bm,
                "map"            => $value->map,
                "qty"            => $value->qty,
                "harga"          => $value->harga,
                "total"          => $value->total,
                "barang"         => $this->tampil_barang($value->ref, 'sj-retur', $jual_retur)
            ];
        }
        foreach ($this->jurnal_umum_msk as $value) {
            $dt_msk_jurnal_umum[] = (object) [
                "tgl"            => $value->tgl,
                "no_akun"        => $value->no_akun,
                "hpp"            => $value->hpp,
                "jenis_jurnal"   => $value->jenis_jurnal,
                "ref"            => $value->ref,
                "bm"             => $value->bm,
                "map"            => $value->map,
                "qty"            => $value->qty,
                "harga"          => $value->harga,
                "total"          => $value->total,
                "barang"         => $this->tampil_barang($value->ref, 'jurnal-masuk', $ju_masuk)
            ];
        }

        foreach ($this->jurnal_umum_klr as $value) {
            $dt_klr_jurnal_umum[] = (object) [
                "tgl"            => $value->tgl,
                "no_akun"        => $value->no_akun,
                "hpp"            => $value->hpp,
                "jenis_jurnal"   => $value->jenis_jurnal,
                "ref"            => $value->ref,
                "bm"             => $value->bm,
                "map"            => $value->map,
                "qty"            => $value->qty,
                "harga"          => $value->harga,
                "total"          => $value->total,
                "barang"         => $this->tampil_barang($value->ref, 'jurnal-keluar', $ju_keluar)
            ];
        }
        // AKHIR JURNAL

        foreach ($dt_beli as $v) {
            foreach ($v->barang as $x) {
               $tmp_beli[] = [
                'tgl'           => $v->tgl,
                'tgl_time'      => strtotime($v->tgl),
                'id_brg'        => $x->id_brg,
                'nama_brg'      => $x->nama_brg,
                'qty'           => $x->qty,
                'harga'         => $x->harga
               ];
            }
        }

        foreach ($dt_jual as $v) {
            foreach ($v->barang as $x) {
               $tmp_jual[] = [
                'tgl'           => $v->tgl,
                'tgl_time'      => strtotime($v->tgl),
                'id_brg'        => $x->id_brg,
                'nama_brg'      => $x->nama_brg,
                'qty'           => $x->qty,
                'harga'         => $x->harga
               ];
            }
        }

        foreach ($dt_beli_retur as $v) {
            foreach ($v->barang as $x) {
                $tmp_beli_retur[] = [
                    'tgl'           => $v->tgl,
                    'tgl_time'      => strtotime($v->tgl),
                    'id_brg'        => $x->id_brg,
                    'nama_brg'      => $x->nama_brg,
                    'qty'           => $x->qty_retur,
                    'harga'         => $x->hpp
                ];
            }
        }

        foreach ($dt_jual_retur as $v) {
            foreach ($v->barang as $x) {
                $tmp_jual_retur[] = [
                    'tgl'           => $v->tgl,
                    'tgl_time'      => strtotime($v->tgl),
                    'id_brg'        => $x->id_brg,
                    'nama_brg'      => $x->nama_brg,
                    'qty'           => $x->qty_retur,
                    'harga'         => $x->hpp
                ];
            }
        }

        foreach ($dt_msk_jurnal_umum as $v) {
            foreach ($v->barang as $x) {
                $tmp_msk_ju[] = [
                    'tgl'           => $v->tgl,
                    'tgl_time'      => strtotime($v->tgl),
                    'id_brg'        => $x->id_brg,
                    'nama_brg'      => $x->nama_brg,
                    'qty'           => $x->qty,
                    'harga'         => $x->harga
                ];
            }
        }

        foreach ($dt_klr_jurnal_umum as $v) {
            foreach ($v->barang as $x) {
                $tmp_klr_ju[] = [
                    'tgl'           => $v->tgl,
                    'tgl_time'      => strtotime($v->tgl),
                    'id_brg'        => $x->id_brg,
                    'nama_brg'      => $x->nama_brg,
                    'qty'           => $x->qty,
                    'harga'         => $x->harga
                ];
            }
        }

        foreach ($opname as $v) {
            $tmp_opname[] = [
                'tgl'           => $v->tgl,
                'id_brg'        => $v->id_brg,
                'nama_brg'      => $v->nama_brg,
                'qty'           => $v->stok_sesudah
            ];
        }

        $tmp_tgl_fix = [];
        $tmp_tgl_fase1 = array_merge($tmp_beli, $tmp_jual, $tmp_beli_retur, $tmp_jual_retur, $tmp_msk_ju, $tmp_klr_ju, $tmp_opname);

        foreach ($tmp_tgl_fase1 as $e) {
            $tmp_tgl_fix[$e['tgl']] = $e['tgl'];
        }
        $tglQ = [];
        foreach (array_sort($tmp_tgl_fix) as $t) {
            $tglQ[] = strtotime($t);
        }

        $cari_tgl = array_filter($tglQ, function ($v) use ($tgl) {
            return $v == $tgl;
        });

        $saklar = 'off';

        if(empty($cari_tgl)) {
            $saklar = 'on';
        }

        $tgl_dekat = (!empty($tmp_tgl_fix)) ? $this->closestnumber( $tgl, array_values($tglQ) ): null; 

        $hasil = 0;
        $awal = 0;

        foreach (array_sort($tmp_tgl_fix) as $v) {
            $stok_beli = $this->get_sisa_detail($tmp_beli, $data_brg->kode, $v);
            $stok_jual = $this->get_sisa_detail($tmp_jual, $data_brg->kode, $v);
            $stok_beli_retur = $this->get_sisa_detail($tmp_beli_retur, $data_brg->kode, $v);
            $stok_jual_retur = $this->get_sisa_detail($tmp_jual_retur, $data_brg->kode, $v);
            $stok_msk_ju = $this->get_sisa_detail($tmp_msk_ju, $data_brg->kode, $v);
            $stok_klr_ju = $this->get_sisa_detail($tmp_klr_ju, $data_brg->kode, $v);
            
            // $log_opname = DB::table('log_opname_stok')
            //                     ->where('tgl', $v)
            //                     ->where('id_brg', $data_brg->kode)
            //                     ->orderBy('created_at', 'DESC')
            //                     ->select('tgl', 'id_brg', 'nama_brg', 'stok_sesudah', DB::Raw('max(created_at) as ct'))
            //                     ->groupBy('tgl', 'id_brg' ,'nama_brg', 'stok_sesudah')
            //                     ->first();

            $log_opname = $this->opname_->where('tgl', $v)
                                        ->where('id_brg', $data_brg->kode) 
                                        ->first();

            if (isset($log_opname)) {
                $awal = $log_opname->stok_sesudah;
                $ketr = '<span class="badge bg-green">opname</span>';
            } else {
                $ketr = '';
            }

            $stok_masuk = ($stok_beli + $stok_msk_ju);
            $stok_keluar = ($stok_jual + $stok_klr_ju);

            $hasil = $awal + ($stok_masuk + $stok_jual_retur ) - ($stok_keluar + $stok_beli_retur);

            $stokQ[] = [
                'tgl'               => $v,
                'tgl_time'          => strtotime($v),
                'id_brg'            => $data_brg->kode,
                'nama_brg'          => $data_brg->nama_brg,
                'stok_beli'         => $stok_beli,
                'stok_jual'         => $stok_jual,
                'stok_beli_retur'   => $stok_beli_retur,
                'stok_jual_retur'   => $stok_jual_retur,
                'stok_msk_ju'       => $stok_msk_ju,
                'stok_klr_ju'       => $stok_klr_ju,
                'stok_awal'         => $awal,
                'stok_masuk'        => $stok_masuk,
                'stok_keluar'       => $stok_keluar,
                'sisa'              => $hasil
            ];
            $awal = $hasil;
        }
        
        $dty = array_filter($stokQ, function ($value) use ($tgl_dekat) {
            return $value['tgl_time'] == $tgl_dekat;  
        });

        $sty = [];

        if (!empty($dty)) {
            foreach ($dty as $s) {
                $sty = [
                    'stok_awl'        => $s['stok_awal'],
                    'stok_msk'        => $s['stok_masuk'],
                    'stok_klr'        => $s['stok_keluar'],
                    'stok_bl_retur'   => $s['stok_beli_retur'],
                    'stok_jl_retur'   => $s['stok_jual_retur'],
                    'sisaa'           => $s['sisa']
                ];

                if ($saklar == 'on') {
                    $sty = [
                        'stok_awl'        => $s['sisa'],
                        'stok_msk'        => NULL,
                        'stok_klr'        => NULL,
                        'stok_bl_retur'   => NULL,
                        'stok_jl_retur'   => NULL,
                        'sisaa'           => NULL
                    ];
                }
            }
        } else {
            $sty = [
                'stok_awl'        => NULL,
                'stok_msk'        => NULL,
                'stok_klr'        => NULL,
                'stok_bl_retur'   => NULL,
                'stok_jl_retur'   => NULL,
                'sisaa'           => NULL
            ];
        }

        return $sty;
    }

    //ex
    public function excel_sj($tgl)
    {
        $tgl_time = (string)strtotime($tgl);

        $data = DB::table('barang')
                        ->where('jenis_brg', 5)
                        // ->whereBetween('kode', [1554,1670])
                        // ->whereBetween('kode', [1671,1800])
                        // ->whereIn('kode', [1554,1555,1556,1557])
                        // ->where('kode', 1555)
                        ->where('status', NULL)
                        ->get();

        $pembelian = $this->pembelian;
        $penjualan = $this->penjualan;
        $beli_retur = $this->beli_retur;
        $jual_retur = $this->jual_retur;
        $ju_masuk = $this->ju_masuk;
        $ju_keluar = $this->ju_keluar;

        // $data = $this->barangQ->where('jenis_brg', 5);

        // $tgl_time_ = (string)strtotime('2021-04-14');
        // // $awal_ = $this->tampil_stok_harian($tgl_time_, 1555)['stok_awl'];
        // $stok_masuk = $this->tampil_stok_harian($tgl_time_, 1555)['stok_msk'];
        // return $stok_masuk;

        // dd($data);
        $logg = [];
        $no = 1;
        foreach ($data as $v) {
            // $log = DB::table('log_opname_stok')
            //                     ->where('tgl', $tgl)
            //                     ->where('id_brg', $v->kode)
            //                     ->orderBy('created_at', 'DESC')
            //                     ->select('tgl','nama_brg', 'stok_sesudah', DB::Raw('max(created_at) as ct'))
            //                     ->groupBy('tgl','nama_brg', 'stok_sesudah')
            //                     ->first();

            $log = $this->opname_->where('tgl', $tgl)
                                 ->where('id_brg', $v->kode) 
                                 ->first();

            // ====================================================
            // $awal_ = $this->tampil_stok_harian($tgl_time, $v->kode)['stok_awl'];
            // $stok_masuk = $this->tampil_stok_harian($tgl_time, $v->kode)['stok_msk'];
            // $stok_keluar = $this->tampil_stok_harian($tgl_time, $v->kode)['stok_klr'];
            // $stok_beli_retur = $this->tampil_stok_harian($tgl_time, $v->kode)['stok_bl_retur'];
            // $stok_jual_retur = $this->tampil_stok_harian($tgl_time, $v->kode)['stok_jl_retur'];
            // ===============================================
            $detail_ = $this->tampil_stok_harian($tgl_time, $v->kode,
                                                    $pembelian, $penjualan, $beli_retur, $jual_retur,
                                                        $ju_masuk, $ju_keluar);
            $awal_ = $detail_['stok_awl'];
            $stok_masuk = $detail_['stok_msk'];
            $stok_keluar = $detail_['stok_klr'];
            $stok_beli_retur = $detail_['stok_bl_retur'];
            $stok_jual_retur = $detail_['stok_jl_retur'];

            // $logg[] = [$awal_, $stok_masuk];

            if ( isset($log) ) {
                $awal_ = $log->stok_sesudah;
            } 

            $hasil = $awal_ + ($stok_masuk + $stok_jual_retur) - ($stok_keluar + $stok_beli_retur);

            $dt[] = (object) [
                'no'                     => $no++,
                'id_brg'                 => $v->kode,
                'nama_brg'               => $v->nama_brg,
                'harga'                  => $v->harga,
                'stok_awal'              => $awal_,
                'stok_masuk'             => $stok_masuk,
                'stok_keluar'            => $stok_keluar,
                'stok_beli_retur'        => $stok_beli_retur,
                'stok_jual_retur'        => $stok_jual_retur,
                'total'                  => ($stok_keluar - $stok_jual_retur)* $v->harga,
                'sisa'                   => $hasil
                // 'tamil_stok_harian'      => $this->tampil_stok_harian($tgl_time, $v->kode)
            ];
        }

        $nama_file = "Rekap SJ ".$tgl.".xlsx";
        return Excel::download(new StokExport($dt), $nama_file);
    }
    // ex
}
