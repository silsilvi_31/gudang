<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;
use Session;
use DB;
use Redirect;
use Validator;
use File;
use Excel;
use App\Exports\GajiRukoExport;

class GajiController extends Controller
{
    public function __construct()
    {
        date_default_timezone_set("Asia/jakarta");
    }

    public function index()
    {
        return view('admin.gaji.index');
    }

    public function datatable()
    {
        $data = DB::table('claimabsen')
                        ->select(DB::raw('sum(total_gaji) as total, count(total_gaji) as total_kar, tgl'))
                        ->groupBy('tgl')
                        ->get();


        return datatables::of($data)
        ->addIndexColumn()
        ->addColumn('opsi', function ($data) {
            $edit = route('gaji.detail_gaji', [($data->tgl)]);
            $tgl = "'".$data->tgl."'";
            return '<a href="'.$edit.'" class="btn btn-sm btn-success">Detail<a/>
                    <button type="button" class="btn btn-sm btn-danger" onclick="delete_claim('.$tgl.')"><i class="fa fa-trash"></i></button>';
            // return 'opsi';
        })
        ->rawColumns(['opsi'])
        ->make(true);
    }

    public function get_nama(Request $req)
    {
        $kodep = $req->_kodep;
        $data_kar = DB::table('karyawan')
                        ->where('kode',  $kodep)
                        ->first();
            
        $nama = isset($data_kar) ? $data_kar->nama : null;
        $data['nama'] = $nama;
        return response()->json($data);
    }

    public function datatable_detail_gaji(Request $req)
    {
        $tgl = date('Y-m-d', strtotime($req->_tgl) );
        $data = DB::table('claimabsen as a')
                                ->leftJoin('karyawan as b', 'a.kodep', '=', 'b.kode') 
                                ->where('tgl', $tgl)->get();

        return datatables::of($data)
        ->addIndexColumn()
        ->addColumn('opsi', function ($data) {
            return 'aksi';
        })
        ->rawColumns(['aksi'])
        ->make(true);

    }

    public function detail_gaji($tgl)
    {
        $data['tgl'] = date('d-m-Y', strtotime($tgl));
        return view('admin.gaji.detail_gaji')->with($data);
    }

    public function delete(Request $req)
    {
        $id_user = session::get('id_user');
        $tgl = date('Y-m-d', strtotime($req->_tgl));

        $res = [];
        $delete = DB::table('claimabsen')->where('tgl', $tgl)->delete();

        if ($delete) {
            $delete_jurnal = DB::table('jurnal')->where('jenis_jurnal', 'gaji')->where('tgl', $tgl)->delete();
            $res = [
                'code' => 300,
                'msg' => 'Data telah dihapus'
            ];
        } else {
            $res = [
                'code' => 400,
                'msg' => 'Gagal dihapus'
            ];
        }
        $data['response'] = $res;
        return response()->json($data);
    }

    public function gaji_mingguan($tgl)
    {
        // $pecah = explode('&', $tgl);
        //   $tgl_awal = $pecah[0];
        //     $tgl_akhir = $pecah[1];  

        $data['tgl_awal'] = $tgl;
        // $data['tgl_akhir'] = $tgl_akhir;

        return view('admin.gaji.gaji_mingguan')->with($data);
    }

    public function cek_gaji($data_claim, $kodep)
    {
        // $dt = array_filter($data_claim->toArray(), function ($dt) use ($kodep){
        //     return $dt->kodep = $kodep;
        // });

        return $data_claim;
    }

    function cari_gaji($data_claim, $kodep, $tgl)
    {
        $cari = [
            'kd' => (string)$kodep,
            'tgl' => $tgl
        ];

        $tmp = [];

        $dt = array_filter($data_claim->toArray(), function ($dt) use ($cari) {
            return $dt->kodep === $cari['kd'] && $dt->tgl === $cari['tgl'];
        });

        foreach ($dt as $k => $v) {
            $tmp = $v;
        }
        
        return $tmp;
    }

    public function datatable_gaji_mingguan(Request $req)
    {
        $tgl_awal = date('Y-m-d', strtotime($req->_tglAwal));
        // $tgl_akhir = date('Y-m-d', strtotime($req->_tglAkhir));
        $tgl_akhir = date('Y-m-d', strtotime('+6 days', strtotime($tgl_awal)));


        $sabtu = date('Y-m-d', strtotime('+1 days', strtotime($tgl_awal)));
        $minggu = date('Y-m-d', strtotime('+2 days', strtotime($tgl_awal)));
        $senin = date('Y-m-d', strtotime('+3 days', strtotime($tgl_awal)));
        $selasa = date('Y-m-d', strtotime('+4 days', strtotime($tgl_awal)));
        $rabu = date('Y-m-d', strtotime('+5 days', strtotime($tgl_awal)));
        $kamis = date('Y-m-d', strtotime('+6 days', strtotime($tgl_awal)));

        $data = DB::table('claimabsen')
                            ->whereBetween('tgl', [$tgl_awal, $tgl_akhir])
                            ->get();

        // $dt_claim[] = [
        //     'kodep' => $data->kodep
        // ];

        $karyawan = DB::table('karyawan')->where('id_jabatan', 5)->get();

        $data_karyawan = [];


        foreach ($karyawan as $value) {
            // dd($this->cek_gaji($data, $value->kode));
            if (empty($this->cek_gaji($data, $value->kode))) {
                $data_karyawan[] = [
                    'kodep' => $value->kode,
                    'nama' => $value->nama,
                    'tgl' => null,
                    'jumat' => null,
                    'sabtu' => null,
                    'minggu' => null,
                    'senin' => null,
                    'selasa' => null,
                    'rabu' => null,
                    'kamis' => null
                ];
            } else {
                $data_karyawan[] = [
                    'kodep' => $value->kode,
                    'nama' => $value->nama,
                    'tgl' => $tgl_awal,
                    'jumat' => $this->cari_gaji($data, $value->kode, $tgl_awal),
                    'sabtu' => $this->cari_gaji($data, $value->kode, $sabtu),
                    'minggu' => $this->cari_gaji($data, $value->kode, $minggu),
                    'senin' => $this->cari_gaji($data, $value->kode, $senin),
                    'selasa' => $this->cari_gaji($data, $value->kode, $selasa),
                    'rabu' => $this->cari_gaji($data, $value->kode, $rabu),
                    'kamis' => $this->cari_gaji($data, $value->kode, $kamis),
                ];
            }
        }


        // dd($data_karyawan['kodep']);
        return Datatables::of($data_karyawan)
        ->addIndexColumn()
        ->addColumn('opsi', function ($data_karyawan) {
            return 'opsi';
        })
        ->editColumn('jumat', function ($data_karyawan) {
            $gj_jumat = empty($data_karyawan['jumat']->total_gaji) ? '-' : $data_karyawan['jumat']->total_gaji;
            return $gj_jumat;
        })
        ->editColumn('sabtu', function ($data_karyawan) {
            $gj_sabtu = empty($data_karyawan['sabtu']->total_gaji) ? '-' : $data_karyawan['sabtu']->total_gaji;
            return $gj_sabtu;
        })
        ->editColumn('minggu', function ($data_karyawan) {
            $gj_minggu = empty($data_karyawan['minggu']->total_gaji) ? '-' : $data_karyawan['minggu']->total_gaji;
            return $gj_minggu;
        })
        ->editColumn('senin', function ($data_karyawan) {
            $gj_senin = empty($data_karyawan['senin']->total_gaji) ? '-' : $data_karyawan['senin']->total_gaji;
            return $gj_senin;
        })
        ->editColumn('selasa', function ($data_karyawan) {
            $gj_selasa = empty($data_karyawan['selasa']->total_gaji) ? '-' : $data_karyawan['selasa']->total_gaji;
            return $gj_selasa;
        })
        ->editColumn('rabu', function ($data_karyawan) {
            $gj_rabu = empty($data_karyawan['rabu']->total_gaji) ? '-' : $data_karyawan['rabu']->total_gaji;
            return $gj_rabu;
        })
        ->editColumn('kamis', function ($data_karyawan) {
            $gj_kamis = empty($data_karyawan['kamis']->total_gaji) ? '-' : $data_karyawan['kamis']->total_gaji;
            return $gj_kamis;
        })
        ->AddColumn('total_gaji_mingguan', function ($data_karyawan){
            $gj_jumat = empty($data_karyawan['jumat']->total_gaji) ? 0 : $data_karyawan['jumat']->total_gaji;
            $gj_sabtu = empty($data_karyawan['sabtu']->total_gaji) ? 0 : $data_karyawan['sabtu']->total_gaji;
            $gj_minggu = empty($data_karyawan['minggu']->total_gaji) ? 0 : $data_karyawan['minggu']->total_gaji;
            $gj_senin = empty($data_karyawan['senin']->total_gaji) ? 0 : $data_karyawan['senin']->total_gaji;
            $gj_selasa = empty($data_karyawan['selasa']->total_gaji) ? 0 : $data_karyawan['selasa']->total_gaji;
            $gj_rabu = empty($data_karyawan['rabu']->total_gaji) ? 0 : $data_karyawan['rabu']->total_gaji;
            $gj_kamis = empty($data_karyawan['kamis']->total_gaji) ? 0 : $data_karyawan['kamis']->total_gaji;

            $total_gaji_mingguan = $gj_jumat + $gj_sabtu + $gj_minggu + $gj_senin + $gj_selasa + $gj_rabu + $gj_kamis;
            return $total_gaji_mingguan;
        })
        ->AddColumn('total_potongan', function ($data_karyawan) {
            $pot_jumat = empty($data_karyawan['jumat']->potongan) ? 0 : $data_karyawan['jumat']->potongan;
            $pot_sabtu = empty($data_karyawan['sabtu']->potongan) ? 0 : $data_karyawan['sabtu']->potongan;
            $pot_minggu = empty($data_karyawan['minggu']->potongan) ? 0 : $data_karyawan['minggu']->potongan;
            $pot_senin = empty($data_karyawan['senin']->potongan) ? 0 : $data_karyawan['senin']->potongan;
            $pot_selasa = empty($data_karyawan['selasa']->potongan) ? 0 : $data_karyawan['selasa']->potongan;
            $pot_rabu = empty($data_karyawan['rabu']->potongan) ? 0 : $data_karyawan['rabu']->potongan;
            $pot_kamis = empty($data_karyawan['kamis']->potongan) ? 0 : $data_karyawan['kamis']->potongan;

            $total_pot = $pot_jumat + $pot_sabtu + $pot_minggu + $pot_senin + $pot_selasa + $pot_rabu + $pot_kamis;
            return $total_pot;
        })
        ->AddColumn('lembur', function ($data_karyawan) {
            $lembur_jumat = empty($data_karyawan['jumat']->lembur) ? 0 : $data_karyawan['jumat']->lembur;
            $lembur_sabtu = empty($data_karyawan['sabtu']->lembur) ? 0 : $data_karyawan['sabtu']->lembur;
            $lembur_minggu = empty($data_karyawan['minggu']->lembur) ? 0 : $data_karyawan['minggu']->lembur;
            $lembur_senin = empty($data_karyawan['senin']->lembur) ? 0 : $data_karyawan['senin']->lembur;
            $lembur_selasa = empty($data_karyawan['selasa']->lembur) ? 0 : $data_karyawan['selasa']->lembur;
            $lembur_rabu = empty($data_karyawan['rabu']->lembur) ? 0 : $data_karyawan['rabu']->lembur;
            $lembur_kamis = empty($data_karyawan['kamis']->lembur) ? 0 : $data_karyawan['kamis']->lembur;

            $total_lembur = $lembur_jumat + $lembur_sabtu + $lembur_minggu + $lembur_senin + $lembur_selasa + $lembur_rabu + $lembur_kamis;
            return $total_lembur;
        })
        ->addColumn('total', function ($data_karyawan) {
            $gj_jumat = empty($data_karyawan['jumat']->total_gaji) ? 0 : $data_karyawan['jumat']->total_gaji;
            $gj_sabtu = empty($data_karyawan['sabtu']->total_gaji) ? 0 : $data_karyawan['sabtu']->total_gaji;
            $gj_minggu = empty($data_karyawan['minggu']->total_gaji) ? 0 : $data_karyawan['minggu']->total_gaji;
            $gj_senin = empty($data_karyawan['senin']->total_gaji) ? 0 : $data_karyawan['senin']->total_gaji;
            $gj_selasa = empty($data_karyawan['selasa']->total_gaji) ? 0 : $data_karyawan['selasa']->total_gaji;
            $gj_rabu = empty($data_karyawan['rabu']->total_gaji) ? 0 : $data_karyawan['rabu']->total_gaji;
            $gj_kamis = empty($data_karyawan['kamis']->total_gaji) ? 0 : $data_karyawan['kamis']->total_gaji;

            $pot_jumat = empty($data_karyawan['jumat']->potongan) ? 0 : $data_karyawan['jumat']->potongan;
            $pot_sabtu = empty($data_karyawan['sabtu']->potongan) ? 0 : $data_karyawan['sabtu']->potongan;
            $pot_minggu = empty($data_karyawan['minggu']->potongan) ? 0 : $data_karyawan['minggu']->potongan;
            $pot_senin = empty($data_karyawan['senin']->potongan) ? 0 : $data_karyawan['senin']->potongan;
            $pot_selasa = empty($data_karyawan['selasa']->potongan) ? 0 : $data_karyawan['selasa']->potongan;
            $pot_rabu = empty($data_karyawan['rabu']->potongan) ? 0 : $data_karyawan['rabu']->potongan;
            $pot_kamis = empty($data_karyawan['kamis']->potongan) ? 0 : $data_karyawan['kamis']->potongan;

            $lembur_jumat = empty($data_karyawan['jumat']->lembur) ? 0 : $data_karyawan['jumat']->lembur;
            $lembur_sabtu = empty($data_karyawan['sabtu']->lembur) ? 0 : $data_karyawan['sabtu']->lembur;
            $lembur_minggu = empty($data_karyawan['minggu']->lembur) ? 0 : $data_karyawan['minggu']->lembur;
            $lembur_senin = empty($data_karyawan['senin']->lembur) ? 0 : $data_karyawan['senin']->lembur;
            $lembur_selasa = empty($data_karyawan['selasa']->lembur) ? 0 : $data_karyawan['selasa']->lembur;
            $lembur_rabu = empty($data_karyawan['rabu']->lembur) ? 0 : $data_karyawan['rabu']->lembur;
            $lembur_kamis = empty($data_karyawan['kamis']->lembur) ? 0 : $data_karyawan['kamis']->lembur;

            $total_lembur = $lembur_jumat + $lembur_sabtu + $lembur_minggu + $lembur_senin + $lembur_selasa + $lembur_rabu + $lembur_kamis;
            $total_pot = $pot_jumat + $pot_sabtu + $pot_minggu + $pot_senin + $pot_selasa + $pot_rabu + $pot_kamis;
            $total_gaji_mingguan = $gj_jumat + $gj_sabtu + $gj_minggu + $gj_senin + $gj_selasa + $gj_rabu + $gj_kamis;

            $total = $total_gaji_mingguan + $total_lembur - $total_pot;
            return $total;            
        })
        ->rawColumns(['opsi'])
        ->make(true);
    }

    public function set_akun_gaji($gaji, $id)
    {
        $tgl_awal = date('d', strtotime($gaji['tgl_awal']));
        $tgl_akhir = date('d F Y', strtotime($gaji['tgl_akhir']));

        $akun[0]['tgl'] = date('Y-m-d');
        $akun[0]['id_item'] = null;
        $akun[0]['no_akun'] = '220';
        $akun[0]['jenis_jurnal'] = 'gaji-mingguan';
        $akun[0]['ref'] = $id;
        $akun[0]['nama'] = 'ina';
        $akun[0]['keterangan'] = $tgl_awal.'-'.$tgl_akhir;
        $akun[0]['map'] = 'd';
        $akun[0]['hit'] = null;
        $akun[0]['grup'] = 1;
        $akun[0]['qty'] = null;
        $akun[0]['harga'] = $gaji['total_gaji'];
        $akun[0]['total'] = $gaji['total_gaji'];

        $akun[1]['tgl'] = date('Y-m-d');
        $akun[1]['id_item'] = null;
        $akun[1]['no_akun'] ='110';
        $akun[1]['jenis_jurnal'] = 'gaji-mingguan';
        $akun[1]['ref'] = $id;
        $akun[1]['nama'] = 'ina';
        $akun[1]['keterangan'] = $tgl_awal.'-'.$tgl_akhir;
        $akun[1]['map'] = 'k';
        $akun[1]['hit'] = null;
        $akun[1]['grup'] = 2;
        $akun[1]['qty'] = null;
        $akun[1]['harga'] =  $gaji['total_gaji'];
        $akun[1]['total'] = $gaji['total_gaji'];

        $insert_jurnal = DB::table('jurnal')->insert($akun);
    }

    public function buat_jurnal(Request $req)
    {
        $id_user = session::get('id_user');
        $tgl_awal = date('Y-m-d', strtotime($req->_tglM));
        $tgl_akhir = date('Y-m-d', strtotime('+6 days', strtotime($tgl_awal)));

        $data = DB::table('claimabsen')
                            ->select(DB::raw('sum(total_gaji) as total'), DB::raw('sum(lembur) as lembur'),  DB::raw('sum(potongan) as potongan'))
                            ->whereBetween('tgl', [$tgl_awal, $tgl_akhir])
                            ->get();

        $log_jurnal = [];
        foreach ($data as $value) {
            $log_jurnal = (object) [
                    'total' => $value->total,
                    'lembur' => $value->lembur,
                    'potongan' => $value->potongan
            ];
        }
        $total_gaji = $log_jurnal->total + $log_jurnal->lembur - $log_jurnal->potongan;

        $dt_log_gaji = [
                            'tgl_awal' => $tgl_awal,
                            'tgl_akhir' => $tgl_akhir,
                            'total_gaji' => $total_gaji,
                            'created_at' => date("Y-m-d H:i:s"),
                            'user_add' => $id_user
        ];

        $cek_log_gaji = DB::table('log_gaji')
                                    ->where('tgl_awal', $tgl_awal)
                                    ->where('tgl_akhir', $tgl_akhir)
                                    ->first();
        
        if (isset($cek_log_gaji)) {
            $res = [
                'code' => '400',
                'msg' => 'Jurnal Sudah dibuat pada tanggal tsb !'
            ];
        } else {
            $insert = DB::table('log_gaji')->insertGetId($dt_log_gaji);
            $this->set_akun_gaji($dt_log_gaji, $insert);
            if ($insert) {
                $res = [
                    'code' => '200',
                    'msg' => 'Jurnal Berhasil dibuat'
                ];
            } else {
                $res = [
                    'code' => '400',
                    'msg' => 'Jurnal Berhasil dibuat'
                ];
            } 
        }
       
        return response()->json($res);
    }

    public function excel_gaji($tgl) 
    {
        // $pecah = explode('&', $tgl);
        // $tgl_m = date('Y-m-d', strtotime($pecah[0]));
        // $tgl_a = date('Y-m-d', strtotime($pecah[1]));

        $tgl_m = date('Y-m-d', strtotime($tgl));
        $tgl_a = date('Y-m-d', strtotime('+6 days', strtotime($tgl)));
        
        $sabtu = date('Y-m-d', strtotime('+1 days', strtotime($tgl_m)));
        $minggu = date('Y-m-d', strtotime('+2 days', strtotime($tgl_m)));
        $senin = date('Y-m-d', strtotime('+3 days', strtotime($tgl_m)));
        $selasa = date('Y-m-d', strtotime('+4 days', strtotime($tgl_m)));
        $rabu = date('Y-m-d', strtotime('+5 days', strtotime($tgl_m)));
        $kamis = date('Y-m-d', strtotime('+6 days', strtotime($tgl_m)));

        $data = DB::table('claimabsen')
                            ->whereBetween('tgl', [$tgl_m, $tgl_a])
                            ->get();
        
        $karyawan = DB::table('karyawan')->get();

        $data_karyawan = [];

        foreach ($karyawan as $value) {
            if (empty($this->cek_gaji($data, $value->kode))) {
                $data_karyawan[] = (object) [
                    'kodep' => $value->kode,
                    'nama' => $value->nama,
                    'tgl' => null,
                    'jumat' => null,
                    'sabtu' => null,
                    'minggu' => null,
                    'senin' => null,
                    'selasa' => null,
                    'rabu' => null,
                    'kamis' => null
                ];
            } else {
                $data_karyawan[] =  (object) [
                    'kodep' => $value->kode,
                    'nama' => $value->nama,
                    'tgl' => $tgl_m,
                    'jumat' => $this->cari_gaji($data, $value->kode, $tgl_m),
                    'sabtu' => $this->cari_gaji($data, $value->kode, $sabtu),
                    'minggu' => $this->cari_gaji($data, $value->kode, $minggu),
                    'senin' => $this->cari_gaji($data, $value->kode, $senin),
                    'selasa' => $this->cari_gaji($data, $value->kode, $selasa),
                    'rabu' => $this->cari_gaji($data, $value->kode, $rabu),
                    'kamis' => $this->cari_gaji($data, $value->kode, $kamis),
                ];
            }
        }

        $dt['rekap'] = $data_karyawan;
        // $dt['tgl_m'] = $tgl_m;
        // $dt['tgl_a'] = $tgl_a;
        // $dt['total'] = $total_sale;

        // dd($dt);


        $nama_file = "Gaji ".$tgl_m."-".$tgl_a.".xlsx";
        return Excel::download(new GajiRukoExport($dt), $nama_file);  
        // return view('admin.gaji.export_gaji')->with($dt);                             
    }
}
